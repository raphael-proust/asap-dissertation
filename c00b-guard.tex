\cleardoublepage
\thispagestyle{empty}

\section*{Declaration of originality}

This dissertation is the result of my own work and includes nothing which is the outcome of work done in collaboration except where specifically indicated in the text.

\bigskip
It is not substantially the same as any that I have submitted, or, is being concurrently submitted for a degree or diploma or other qualification at the University of Cambridge or any other University or similar institution. I further state that no substantial part of my dissertation has already been submitted, or, is being concurrently submitted for any such degree, diploma or other qualification at the University of Cambridge or any other University of similar institution.

\bigskip
This dissertation does not exceed the regulation length of 60,000 words, including tables and footnotes.

\newpage
\thispagestyle{empty}

\section*{Acknowledgements}

\vfill

Thanks go first to my supervisor Alan.
I would not dare to count how many unfinished versions of this dissertation Alan has read.
For this dissertation and for every report and article Alan tirelessly provided barely-conceivable scrutiny: careful reading, thorough annotations, patient discussion of versions after versions after versions of the same text.

And then there are all the other ways in which Alan help me.
The peripheral aspects of academia, the administrative checkpoints, the linguistic hurdles, etc.
In fact, I invite the reader to mentally add, to each of the thanks below, “and Alan, too, help with that.”

\medskip
There have several occasions during the last four years when I doubted there would ever be a conclusion.
On these occasions, my friends and family carried me forward.
None did it as often as {\mypippafont\large Pippa}, none as lovingly as {\mypippafont\large Pippa}, none as caringly as {\mypippafont\large Pippa}.

Were it not for them and for her, I would have stopped half-way through, and then again three quarters of the way through, and then again a few more times.
There would have only been a draft bit-rotting somewhere or other.

\medskip
Support also came from the whole Cambridge machinery: a complex arrangement of people and administrative entities that make life and work easy and pleasant.
From the buttery staff to the porters of Magdalene.
From the happy hour team to the Graduate Education department of the Computer Laboratory.
The people who made these past four years of my life a Cantabridgean experience in all its comfort are too many for a comprehensive list.
Lise, Bogdan, Anna, Steve, Andy, Anil are the tip of a gigantic iceberg of people smoothing administrative hurdles, manning the backstage of my social gatherings and so much more.
They contributed to this dissertation in innumerable, if indirect, ways.

\vfill
\newpage

~
\vfill

Other people have contributed directly to this dissertation.
Guillaume, the powerful \LaTeX{} wizard, helped me transform a poorly typeset, overflowing heap of words into a reasonably readable document – any remaining æsthetically unpleasant feature is solely due to my own stubbornness in the face of sound advice.
Hannes, Jean and Simon read through an unfinished version of this dissertation and provided detailed feedback.
Their help was precious.

Stephen shared an office with me.
His vast knowledge of compilers, programming languages, natural languages, English grammar, \LaTeX{} syntax, Unicode and so much more has been helpful in many ways.
And even more helpful was his putting up with my incessant questions: “Is there a word for this?” and “Which is more idiomatic of these two forms?” and “Where is the stack frame layout stored in Haskell programs?” and so on and so forth.

\medskip
Finally, in no specific order and for a myriad of reasons, I thank all the friends who brightened my days, challenged my mind, pushed me forward and carried me in meaningful ways.\\
{\addfontfeature{Ligatures=Discretionary}Alan}, {\addfontfeature{Ligatures=Discretionary}Alex} and {\addfontfeature{Ligatures=Discretionary}Alex} for more than they know, more than they would venture to guess, and more than I could say within the sixty thousand words of a dissertation.\\
{\addfontfeature{Ligatures=Discretionary}Chris} for my first, Britishly awkward friendship and for making sure I made my way back home safely that night I got drunk and could not cycle.\\
{\addfontfeature{Ligatures=Discretionary}Tiia} for all of the tellies and laughs and for pointing out all the mistakes I made – and continue to make – when I speak English.\\
{\addfontfeature{Ligatures=Discretionary}Georgie} and {\addfontfeature{Ligatures=Discretionary}Chris} for ruling together. It was fun whilst it lasted.\\
The bears for the breakfasts, teas, musics, outings, etc.\\
{\addfontfeature{Ligatures=Discretionary}Jon} and the Owl Knights for rolling dice.\\
All the French computer scientists partaking in the camelborne invasion of Cambridge.

\vfill
\newpage

\section*{Abstract}

Today, there are various ways to manage the memory of computer programs: garbage collectors of all kinds, reference counters, regions, linear types – each with benefits and drawbacks, each fit for specific settings, each appropriate to different problems, each with their own trade-offs.

Despite the plethora of techniques available, system programming (device drivers, networking libraries, cryptography applications, etc.) is still mostly done in C\@, even though memory management in C is notoriously unsafe.
As a result, serious bugs are continuously discovered in system software.

In this dissertation, we study memory management strategies with an eye out for fitness to system programming.

First, we establish a framework to study memory management strategies.
Often perceived as distinct categories, we argue that memory management approaches are actually part of a single design space.
To this end, we establish a precise and powerful lexicon to describe memory management strategies of any kind.
Using our newly established vocabulary, we further argue that this design space has not been exhaustively explored.
We argue that one of the unexplored portion of this space, the static-automatic gap, contributes to the persistence of C in system programming.

Second, we develop \textsc{asap}: a new memory management technique that fits in the static-automatic gap.
\textsc{Asap} is fully automatic (not even annotations are required) and makes heavy use of static analysis.
At compile time it inserts, in the original program, code that deallocates memory blocks as they becomes useless.
We then show how \textsc{asap} interacts with various, advanced language features.
Specifically, we extend \textsc{asap} to support polymorphism and mutability.

Third, we compare \textsc{asap} with existing approaches.
One of the points of comparison we use is the behavioural suitability to system programming.
We also explore how the ideas from \textsc{asap} can be combined with other memory management strategies.
We then show how \textsc{asap} handles programs satisfying the linear or region constraints.
Finally, we explore the insights gained whilst developing and studying \textsc{asap}.

\newpage

\section*{Résumé}

%\makeatletter
%\language=\l@french
%\makeatother

\begin{otherlanguage}{frenchb}

De nombreuses techniques sont disponibles pour gérer la mémoire des programmes informatiques~: ramasse-miettes, compteur de références, système de régions, système de types linéaires.
Chaque méthode possède ses propres avantages et inconvénients, chaque méthode se prête plus ou moins bien à différents programmes.

Malgré l'abondance de techniques disponibles, le code système (pilotes de périphériques, implémentations de protocoles de communication, logiciels de chiffrement, etc.) est encore et toujours écrit en C\@.
Ceci, bien que C soit notoirement dangereux en matière de gestion de la mémoire.
De ce fait, on découvre régulièrement, dans le code système, d'innombrables bogues.

Dans cette dissertation, nous étudions la gestion de la mémoire dans le con\-texte du code système.

Tout d'abord, nous établissons un lexique précis pour décrire les techniques de gestion de la mémoire.
Parce que lexique s'applique à la multitude de techniques connues qui, jusqu'à présent étaient considérées comme distinctes, nous défendons l'idée que ces techniques font partie d'un même ensemble.
L'étude de cet ensemble révèle des lacunes dont l'une, que nous nommons le «~déficit statique-automatique,~» nous intéresse plus particulièrement.
Et nous défendons l'idée que le déficit statique-automatique contribue à la persistance de C dans le domaine du code système.

Dans un deuxième temps, nous présentons \textsc{asap}~: une nouvelle technique de gestion de la mémoire qui comble le déficit statique-automatique.
\textsc{Asap} analyse les programmes afin d'y insérer, pendant la compilation, des instructions qui, pendant l'exécution, libèrent les blocs de mémoire au moment opportun.
Nous étendons ensuite \textsc{asap} pour lui permettre de gérer la mémoire de programmes avec polymorphisme et mutation.

Dans un troisième temps, nous comparons \textsc{asap} aux autres techniques de gestion de la mémoire et, lorsque c'est possible, nous présentons des solutions hybrides qui empruntent certaines idées d'\textsc{asap} et les intègrent à ces autres méthodes.
Nous observons ensuite la gestion, par \textsc{asap}, de la mémoire de programmes linéaires et à région.
Enfin, nous étudions les liens qui existent entre, d'une part, la gestion de la mémoire en général et \textsc{asap} en particulier et, d'autre part, la gestion des ressources dans d'autres domaines de l'informatique.

\end{otherlanguage}


\nonfrenchspacing

\newpage

\section*{Preface}

This dissertation is organised as follows:

\begin{itemize}
	\item[Part A:] ~
		\begin{itemize}
			\item[Chapter~\ref{chap:introduction}:]
				We discursively lay out the context of our work: memory management and system programming.
				We focus on the interactions between these two domains.
			\item[Chapter~\ref{chap:prerequisites}:]
				We provide the necessary technical background – mathematical,\linebreak[4] notational, lexical and otherwise.
				Note that this chapter essentially collects concepts published by others.
		\end{itemize}
	\item[Part 1:] Design Space
		\begin{itemize}
			\item[Chapter~\ref{chap:design-space}:]
				We explore the design space of memory management strategies.
				To that end, we develop an original lexicon which lets us describe existing me\-mory management strategies with an informative point\linebreak[4] of view.
		\end{itemize}
	\item[Part 2:] \textsc{Asap}
		\begin{itemize}
			\item[Chapter~\ref{chap:paths}:]
				We formally define paths: compile-time descriptions of heap structures.
				We use them to approximate heap structures in a bounded-size fashion during analyses.
				We also use them to synthesise code that scans through the described heap structures during execution.
				Note that, whilst the formalisation detailed in this dissertation is our own, it is influenced by the work of Khedker, Sanyal and Karkare \cite{heapref}.
			\item[Chapter~\ref{chap:asap}:]
				We present \textsc{asap}, a new automatic memory management strategy.
				We give full technical details, from analysis to code transformation.
			\item[Chapter~\ref{chap:extensions}:]
				We introduce new language features, namely: mutability and polymorphism.
				These additions raise a number of challenges which are listed and addressed one by one.
			\item[Chapter~\ref{chap:implementation}:]
				We briefly describe our prototype implementation of \textsc{asap}.
		\end{itemize}
	\item[Part 3:] Recontextualisation
		\begin{itemize}
			\item[Chapter~\ref{chap:comparison}:]
				We revisit the design space of memory management strategy, with a focus on \textsc{asap}.
				Specifically, we compare \textsc{asap} with other approaches.
				Additionally, we show how to hybridise \textsc{asap} and other approaches together.
			\item[Chapter~\ref{chap:subsumptions}:]
				We show that \textsc{asap} subsumes both linear type systems and region-based memory management.
				That is, when given a linear or region-based program, \textsc{asap} emits deallocation instructions similar to the ones generated by the linear or region-based memory manager.
			\item[Chapter~\ref{chap:further}:]
				We explore insights from the comparisons and subsumptions mentioned above.
				These insights range over other forms of resources managements such as registers and system resources (e.g., file descriptors).
		\end{itemize}
	\item[Part Ω:] ~
		\begin{itemize}
			\item[Chapter~\ref{chap:conclusion}:]
				We collect concluding remarks.
		\end{itemize}
\end{itemize}


\newpage

\tableofcontents

\newpage
