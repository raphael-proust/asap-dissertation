\chapter{Insights}
\label{chap:further}

Studying \textsc{asap} casts light on other areas of computer science and on resource management in compilers specifically.
We discuss these insights discursively below.


\section{Caller- and callee-save registers}

Of particular interest is insight linking the linear and region regimes with register use convention.

Focusing on function calls we note that under linear type systems, the whole responsibility for the management of the arguments' memory is given to the callee.
As a result, if both caller and callee need to perform operations, deep-copying is necessary.
Specifically, in a linear program if a caller needs to continue using a value after a call, it needs to make a copy before the call.
Dually, under region regimes, the responsibility for the management of the memory is kept by the caller.
If a callee wants to make an argument escape its region, it needs to make a copy of it.
This is visible in the examples given in the Introduction (Figures~\ref{fig:lin-ex} and~\ref{fig:reg-ex}).

Note how, in the linear case, the caller is responsible for copying the values that need to outlive the call whilst, in the region case, the callee is responsible for the copy.
This is similar to the way registers fall into two separate categories: caller- and callee-save.
In the former, the caller is responsible for copying the value of the register (onto the stack), in the latter the callee is.

Registers are a special kind of resource: their number is predetermined and small.
Additionally, registers are flat: there is no aliasing, nor any form of structure to worry about.
By contrast, vast amounts of memory can be requested from the operating system and they can contain structured data.
Despite these differences, there are similarities in the way that responsibility is split between callers and the callees.
One of the roles of resource management strategies (whether for registers or memory) is to arbitrate this agreement between caller and callee.
\textsc{Asap} arbitrates this agreement based on decorations inferred by static analyses, Rust arbitrates it based on annotations provided by the programmer.

We now hypothesise how this insight can be applied to compiler writing.
By carrying \textsc{asap}-style decorations or Rust-style annotations all the way to the back-end of the compiler, the register allocator could decide what temporaries are caller- and callee-save.
Such a register allocator would select the flavour of register based on information that relates to function calls at a higher-level of reasoning.
This contrasts with current register allocators, often based on local analysis of register use, often ignoring costly inter-procedural information.

The impact this change would have on register allocators is unknown.
One expected impact is about compilation time: inter-procedural register allocation can be costly.
Re-using inferred decorations or written annotations would presumably be more efficient than performing inter-procedural analysis.
We do not know whether the change would also result in more efficient allocations.
Future research is necessary to confirm the improved performance and investigate the potential increased precision.



\section{Liveness vs scope}

An important choice in the design of \textsc{asap} is the use of the $\mathit{Access}$ property to approximate usefulness.
We noted in Chapter~\ref{chap:asap} how this approximation gives the earliest deallocations but not necessarily the most efficient in terms of \textsc{cpu} usage.
We presented an alternative where the deallocations are delayed so as to reduce the amount of scanning: trading \textsc{cpu} work for timeliness.

Placing these alternatives in the context of the subsumptions presented in Chapter~\ref{chap:subsumptions}, we note that $\mathit{Access}$ is an exact match for the linear-based memory management.
That is because the constraints of linear types are designed to make liveness the exact characterisation of non-waste.
On the other hand, note how the translation from region-based µL into standard µL requires the introduction of the $\mathit{tickle}$ function.
This function artificially extends the $\mathit{Access}$ property of its argument.
Also note that calls to $\mathit{tickle}$ on a region $\rho$ are introduced at the end $\rho$'s scope to avoid early deallocations of values.

This points to another correspondence: linear type systems are based on liveness whilst regions are based on scope.
We can see how this insight relates to resource management in different programming languages.

\subsection{Region-like file-descriptor management}

%TODO: mention RAII

Consider official explanations for the $\texttt{defer}$ statement in Go\footnote{\url{https://blog.golang.org/defer-panic-and-recover}}.
It is based on an example of managing file descriptors – more specifically: ensuring they are closed once they are not needed.
Deferred statements are executed when the function returns, or, in other words, when the scope of inner variables ends.
(The main point of deferred statements is that they are executed even if the program $\texttt{panic}$s – the Go equivalent of raising an exception.)
These deferred statements place file descriptor management under the region style management.

Similarly, in programming languages with higher-order functions, it is common for libraries to provide abstraction over resources in the following way\footnote{See, e.g., $\texttt{with\_file}$ and $\texttt{with\_connection}$ in \url{http://ocsigen.org/lwt/2.5.1/api/Lwt_io} or $\texttt{withConnection}$ in \url{https://www.playframework.com/documentation/2.0.2/api/scala/index.html\#play.api.db.DB\$}.}: \texttt{with\_file: file\_name -> (file\_descr -> 'b) -> 'b}.
During the execution of the call $\texttt{with\_file}\: n\: f$, the file named $n$ is opened and its descriptor is passed to the function $f$.
When $f$ returns, the function $\texttt{with\_file}$ closes the file descriptor and passes $f$'s return value along.
Using an anonymous function as the second argument highlights how this is a scope-based approach to resource management: $\texttt{with\_file}\: n\: \texttt{(}\texttt{fun}\: fd\: \texttt{->}\: \dots\: \texttt{)}$.
(Note however, that in many of these libraries, it is the programmer's responsibility to ensure the file descriptor does not escape.
By contrast, region regimes are enforced by a type-like analyser that prevents such escapes.)

\subsection{Linear-like file-descriptor management}

Using linear types for managing file descriptors (and other similar resources) could be useful.
In such a scheme, each file descriptor is consumed by functions such as \texttt{write} and \texttt{read}.
This is similar to the way Clean and Mercury handle \textsc{i/o}: with a linearly typed value\footnote{In this context, linear types are referred to as \emph{unique} or \emph{uniqueness} types.} referred to as “the world”.
Every I/O related function of the standard library expects the special, linearly-typed world.
This argument is consumed and a new representative for the world is returned – along with the result of the operation.

Instead of managing the whole \textsc{i/o} system with one world value, a language could offer a linearly typed representation of each external resource (file descriptor, socket, etc.).
This gives a better granularity than the unique-world technique: separate threads can concurrently write on distinct file descriptors.


\section{Linear types with alias patterns}

Observe that, under linear type systems, it is possible to deallocate values\linebreak[3] with\-out performing any scanning because of the linear constraint.
However these constraints are too tight: they disallow some programs even though their memory can be managed as well as that of a linear program.

\textsc{Asap} lets us explore this area by, first, analysing and transforming an almost but not quite linear program and, second, checking that all deallocations happen without scanning.
These programs, whilst not linear enough for the linear type systems, are linear enough for \textsc{asap} to handle efficiently.
One such not-quite-linear form is programs under affine types: they are handled by \textsc{asap} without resorting to scanning.
Affine types are a weaker variation of linear types.
Thus, by finding other code patterns that are not-quite-linear, we might uncover leads towards new variations of linear types.

One such not-quite-linear form is a restricted use of \emph{alias patterns}.
Alias patterns are available in \textsc{ml} (using the $\texttt{as}$ keyword), Haskell (using the $\texttt{@}$ symbol) and Scala (also using $\texttt{@}$).
As their name suggests, alias patterns create aliasing: they allow the programmer to give an additional name to parts or the whole of the matched value.
To the best of our knowledge, there are no extensions nor weakenings of linear types that handle alias patterns.
However, when used in a specific way (see below), alias patterns are handled by \textsc{asap} as gracefully as linear code – i.e., without incurring any form of scanning.

Consider the example in Figure~\ref{fig:rollback-able}.
The code does not satisfy linear constraints.
(One of the breaches of linearity happens at $\circled{0}$: the value $\mathit{hys}$ is not used.
Similarly, the value $\mathit{hxs}$ is not used at  $\circled{1}$.
However, these are handled by affine types which we discussed before.)
Of particular interest is the alias pattern and its use: both $\mathit{hxs}$, $\mathit{txs}$ and $\mathit{nxs}$ are used.
However, in the branch where $\mathit{nxs}$ is used (at $\circled{2}$), neither of the two others are – and vice versa.
As a result, \textsc{asap} is able to handle the code without scanning.

\begin{figure}
\begin{displaymath}
\begin{array}{l}
	\mathit{maxMerge}\texttt{(}\mathit{xs}\texttt{,}\: \mathit{ys}\texttt{)}\: \texttt{=}\\
	\qquad \texttt{match}\: \mathit{xs}\: \texttt{with}\\
	\qquad \texttt{[}\: \mathit{Cons}\: \texttt{\{}\mathit{Head}\: \texttt{=}\: \mathit{hxs}\texttt{;}\: \mathit{Tail}\: \texttt{=}\: \mathit{txs}\texttt{\}}\: \texttt{as}\: nxs\: \texttt{->}\\
	\qquad \qquad \texttt{match}\: \mathit{ys}\: \texttt{with}\\
	\qquad \qquad \texttt{[}\: \mathit{Cons}\: \texttt{\{}\mathit{Head}\: \texttt{=}\: \mathit{hys}\texttt{;}\: \mathit{Tail}\: \texttt{=}\: \mathit{tys}\texttt{\}}\: \texttt{->}\\
	\qquad \qquad \qquad \texttt{let}\: \mathit{zs}\: \texttt{=}\: \mathit{maxMerge}\texttt{(}\mathit{txs}\texttt{,}\: \mathit{tys}\texttt{)}\: \texttt{in}\\
	\qquad \qquad \qquad \texttt{if}\: \mathit{hxs}\: \texttt{>=}\: \mathit{hys}\: \texttt{then}\\
	\qquad \qquad \qquad \qquad \texttt{let}\: r_0\: \texttt{=}\: \mathit{Cons}\: \texttt{\{}\mathit{Head}\: \texttt{=}\: \mathit{hxs}\texttt{;}\: \mathit{Tail}\: \texttt{=}\: \mathit{zs}\texttt{\}}\: \texttt{in}\\
	\qquad \qquad \qquad \qquad \texttt{return}\: r_0{}^{\circled{0}}\\
	\qquad \qquad \qquad \texttt{else}\\
	\qquad \qquad \qquad \qquad \texttt{let}\: r_1\: \texttt{=}\: \mathit{Cons}\: \texttt{\{}\mathit{Head}\: \texttt{=}\: \mathit{hys}\texttt{;}\: \mathit{Tail}\: \texttt{=}\: \mathit{zs}\texttt{\}}\: \texttt{in}\\
	\qquad \qquad \qquad \qquad \texttt{return}\: r_1{}^{\circled{1}}\\
	\qquad \qquad \texttt{|}\: \mathit{Nil}\: \texttt{->}\\
	\qquad \qquad \qquad \texttt{return}\: \mathit{nxs}{}^{\circled{2}}\\
	\qquad \qquad \texttt{]}\\
	\qquad \texttt{|}\: \mathit{Nil}\: \texttt{->}\\
	\qquad \qquad \texttt{return}\: ys\\
	\qquad \texttt{]}\\
\end{array}
\end{displaymath}
\caption{Using alias patterns}
\label{fig:rollback-able}
\end{figure}


We have not formalised the use of alias patterns within a linearly typed program.
We posit that, as long as not both of the alias (in our example $\mathit{nxs}$) and the components (in our example $\mathit{hxs}$ and $\mathit{txs}$) are used, the memory can be managed efficiently.

%TODO? schrodinger's aliasing: you create aliases and both are available except that when you read one, the other becomes dead.


\section{Design space tetrahedron}


Designing \textsc{asap} and comparing it to existing approaches sheds light onto new aspects of the design space explored in Chapter~\ref{chap:design-space}.
Of particular importance, the comparison of \textsc{asap} with linear and region regimes highlighted the need for a more precise actuator differentiation.
Indeed, consider how linearly typed programs are handled by \textsc{asap}: after minimal changes to its general purpose analyses, \textsc{asap} gives the optimal results we expect from linear types and region systems.
Even though both linear types and region regimes are strategies that rely heavily on the compiler, they differ significantly from \textsc{asap}.
We define two new actuators, the combination of which subsumes the compiler actuator.
\begin{description}
	\item[Type-like analyser] The \emph{type-like analyser} actuator is a component of the compiler that checks the program follows a certain form.
		\index{type-like analyser}
		In other words, this actuator implements type-like analyses: if they succeed, the program is compiled, otherwise the compilation is interrupted.
	\item[Transformer] The \emph{transformer} actuator is a component of the compiler that modifies the program.
		\index{transformer}
		Note that the transformer can run some analyses in order to decide how to transform what part of the program.
		However, these are not type-like in that their result is richer than a single boolean.
\end{description}

Note that \textsc{asap} is mostly transformer-driven but has a small type-like component.
Indeed, remember that one of the hypothesis \textsc{asap} makes about µL is well-typedness.

This enriched design space can be represented as an actuator tetrahedron as per Figure~\ref{fig:actuator-tetra} (instead of the triangle of Chapter~\ref{chap:design-space}).
The strategies (omitted for clarity) can be placed in the tetrahedron such that proximity to a vertex indicates how much the actuator contributes to the strategy.
This tetrahedron can be projected onto the actuator triangle of Chapter~\ref{chap:design-space}.

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{figs/Triangle-Tetra.pdf}
	\caption{The actuator tetrahedron with projection}
	\label{fig:actuator-tetra}
\end{figure}
