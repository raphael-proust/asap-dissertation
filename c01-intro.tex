\chapter{Introduction}
\label{chap:introduction}

Computer Science can be described as the art of designing, implementing and composing abstractions.
These abstractions bridge a gap between fast and precise but dumb processing machines (also known as computers) on the one hand and slow and error-prone but meta-cognisant organisms (also known as humans) on the other hand.
With no abstractions, it is very difficult for humans to make use of computers.

This description is especially true of Programming Language research which provides a specific type of abstractions called \emph{programming languages} to a specific class of humans called \emph{programmers}.
A programming language is a set of abstractions and tools that can be used to create programs.
\index{programming language}
A programming language is said to be low-level if its abstractions are few and expose the specificities of the underlayer.
\index{programming language!low-level}
Conversely, a programming language is said to be high-level if its abstractions are many and hide entirely their underlayer.
\index{programming language!high-level}

In this dissertation we are concerned with one abstraction in particular: memory.
At a low level of abstraction, memory is a sequence of bits that can hold either of two values: 0 or 1.
Programming languages provide abstractions on top of this low level view.
These abstractions let programs and programmers store and retrieve values from the memory: the values are encoded into sequences of 0 and 1 which are copied onto and loaded from the memory.
One aspect of this process of storing and retrieving values is to decide where in the memory – i.e., at what offset of the sequence of bits – is any particular value stored.
This aspect is further complicated by time-sharing: the concurrent execution of several programs on a single machine.
Which program uses what part of the memory?
How do programs reserve a section of the memory for their own use?
This bookkeeping, both within a program to decide where each value is stored and between programs to decide which one has exclusive access to what part of the memory, is called \emph{memory management}.

We study memory management: how programs and programmers reserve and release (allocate and deallocate) the memory of the computer during the execution of a program.
Even more specifically, this dissertation introduces \textsc{asap}, a new method to deallocate memory where the \emph{compiler} is the main actor.
With \textsc{asap}, memory is entirely abstracted away from programmers who only need to deal with values, not their storage.
In that respect, \textsc{asap} differs from C (where programmers manage storage) and feels more like a garbage collector (i.e., it is fully automatic).
However, it differs from garbage collectors in that the compiler (rather than the runtime) manages storage.

We argue that \textsc{asap} possesses a unique combination of qualities.
We further argue that this unique combination makes \textsc{asap} a prime option for managing the memory of a specific category of programs: system software.


\section{Compilers}

Compilers transform code.
They start from a high-level form readable and writeable by humans.
They produce a low-level form executable by machines.
For engineering purposes, compilation happens in several stages: from source code (a stream of characters) into an abstract syntax tree into a series of intermediate representations (\textsc{ir}) into a binary executable format (a stream of bytes or bits).
\index{abbreviation!\textsc{ir}: intermediate representation}
This pipeline view of compilers is presented in Figure~\ref{fig:compiler-pipeline}

\begin{figure}
\centering
	\begin{tikzpicture}[align=center,node distance=1.5cm]
		\node(src)[draw, align=left]{\texttt{source} \\ \texttt{code}};
		\node(parsetree)[align=center, right of=src]{parse \\ tree};
		\node(ir1)[right of=parsetree]{\textsc{ir}$_1$};
		\node(ir2)[right of=ir1]{$\dots$};
		\node(ir3)[right of=ir2]{\textsc{ir}$_n$};
		\node(assembly)[draw, align=left, right of=ir3]{\texttt{binary} \\ \texttt{code}};
		\draw[->] (src) -- (parsetree);
		\draw[->] (parsetree) -- (ir1);
		\draw[->] (ir1) -- (ir2);
		\draw[->] (ir2) -- (ir3);
		\draw[->] (ir3) -- (assembly);
	\end{tikzpicture}
\caption{A compiler pipeline}
\label{fig:compiler-pipeline}
\end{figure}

The initial part of the compiler pipeline parses the source code and lightly transforms the result.
It is called the \emph{front-end}.
\index{front-end}
The central part is responsible for the bulk of the transformation and is called the \emph{middle-end}.
\index{middle-end}
It uses a series of \textsc{ir}s: a gradient of languages between the source and target languages.
The terminal part of the pipeline is called the \emph{back-end} and is responsible for the transformations that are specific to the target architecture.
\index{back-end}

In order to translate a program from one \textsc{ir} to the next, the compiler needs information about the program.
The compiler runs analyses on the code to gather this information.
Some analyses exist to enforce specific correctness criteria.
These \emph{type-like} analyses return a simple yes-or-no value which determines whether the compilation should continue or not.
\index{analysis!type-like}
E.g., type checking is a type-like analysis: it distinguishes type-correct programs that the compiler processes and type-incorrect programs that the compiler rejects.
Other analyses merely inform the next phase of the compilation.
E.g., compilers analyse the liveness of variables to safely choose which registers to coalesce.



\section{Memory management}

Memory management is the set of techniques and abstractions used by programmers to manage storage for a program's values.
\index{memory management}
Different approaches, each with their own advantages and drawbacks, are used in different programming languages.
We list here approaches that are widely deployed or otherwise interesting to our study.


\subsection{Thin abstraction \emph{à la} C}

In low-level languages it is the programmers' responsibility to allocate and deallocate memory.
In C specifically, the only tools available are the five functions \texttt{malloc}, \texttt{free}, \texttt{calloc}, \texttt{realloc} and \texttt{alloca}.
These offer a thin abstraction – compared to the approaches listed below.

The main advantage of the C approach is that it offers prodigious control to the programmers.
However, its main advantage brings about its greatest weakness: it places the full burden of responsibility on the programmers\footnote{As Benjamin Parker remarked “With great power there must also come great responsibility!”}.

Often, programmers promote C-like approach by invoking control over the timing of deallocation.
However, an arguably more important aspect of this control concerns the layout of values.
Specifically, C does not impose any restrictions on how data is represented in memory: the programmers decide, say, whether a nested struct is represented inline or as a pointer to a separate block.
With this feature, programmers can decide to represent \textsc{tcp} segments in memory with the same sequence of bits as on the wire or in the air.

Whilst it provides a lot of control over the program execution behaviour, C's approach imposes on the programmers the full burden of responsibility.
Specifically, the programmers must ensure two correctness criteria:
\begin{itemize}
	\item the program does not read from nor write to a pointer to unallocated or deallocated space, and
	\item after values become useless, they are eventually deallocated once.
\end{itemize}

These criteria guarantee there are no crashes induced by memory management during execution.
They also guarantee the program occupies only marginally more memory than is strictly necessary.

\subsection{Garbage Collection}

In most modern programming languages (\textsc{ml}, Java, Go, etc.), the main program, called the \emph{mutator}, is linked with a generic piece of code known as the \emph{garbage collector} (\textsc{gc}).
\index{abbreviation!GC@\textsc{gc}: garbage collector, garbage collection}
\index{garbage collector}
\index{garbage collection}
\index{mutator}
The mutator performs the computations specified by the programmers, all the while effectively leaking memory.
Once the heap is full, the mutator cedes control to the \textsc{gc}.
The \textsc{gc} systematically explores the heap starting from global values and variables on the stack (known as \emph{roots}) and discards anything that is unreachable.
(We discuss variations on this design below.)

Note that unreachability is merely a \emph{proxy} for uselessness: a value that is unreachable (i.e., cannot be accessed) is useless (i.e., will not be accessed).
However, this is only an approximation: in most programs, there are many values that are useless but still reachable – as shown by Röjemo and Runciman \cite{drag}.

The main advantage of \textsc{gc}s is reliability: they have been debugged into correctness, preventing both crashes and memory leaks.

That correctness comes at a cost: loss of control.
More specifically, because \textsc{gc}s systematically explore the memory graph, they need to determine the size of memory blocks and distinguish pointers and integers during execution.
Thus, \textsc{gc}s rely on \emph{runtime-types}\footnote{\textsc{ml} – and the like – are known for their type-erasure semantics: it is not possible at runtime to distinguish an integer (\texttt{int}) from a character (\texttt{char}).
Whilst user-level types are indeed erased, runtime types remain.
They are necessary, so that the \textsc{gc} can distinguish arrays from integers from pointers.} (also known as \emph{tags}) and impose a fixed pattern for the memory representation of values – alternatives, some of which do not restrict layout, are presented below.
\index{runtime-type}
\index{tag}


\paragraph{Conservative \textsc{gc}s}

In order to avoid the loss of control over value representation, \emph{conservative} \textsc{gc}s (\textsc{cgc}s), such as the one developed by Hans-Juergen Boehm \cite{cons-gc}, were created.
\index{garbage collector!conservative}
\index{abbreviation!CGC@\textsc{cgc}:conservative garbage collector}
\index{abbreviation!CGC@\textsc{cgc}:conservative garbage collection}
A conservative \textsc{gc} scans the heap without any information about the layout of the memory.
As a result, the conservative \textsc{gc} must make a conservative (hence the name) assumption about the words in memory: each word is treated as a pointer because it might happen to be one.
(A \textsc{gc} that is not conservative is called \emph{precise}.)
\index{garbage collector!precise}

Whilst \textsc{cgc}s do not restrict value representation\footnote{In fact, \textsc{cgc}s requires pointers to be word-aligned which is almost always the case in practice.}, they do instead abandon one of the correctness criteria.
Specifically, under a \textsc{cgc}, not all memory blocks are reclaimed.

Additionally, \textsc{cgc}s do not support compaction (a technique by which \textsc{gc}s move blocks in the heap so they are closer together).
Specifically, under \textsc{cgc}s, it is not possible to move a block and update the pointers to that block.
Indeed, the \textsc{cgc} cannot tell which are pointers to the block and which are word-sized values that happen to correspond to the address of the block.


\paragraph{Tagless \textsc{gc}s}

\emph{Tagless} \textsc{gc}s \cite{tagless} are a family of \textsc{gc} which do not use runtime-type information.
\index{garbage collector!tagless}
Instead, tagless \textsc{gc}s reconstruct the necessary type information from hints sprinkled on the execution stack: the type of values stored at each offset of each frame.
This information is collected by the compiler and carried through to the final binary for the \textsc{gc} to use.

Tagless \textsc{gc}s are not widely deployed.
The current trend in \textsc{gc} development is to make them simpler and faster with a focus on avoiding long pauses.
Specifically, there is a preference towards spending a little more resources in the mutator (setting up tag bits and such) if it significantly simplifies the \textsc{gc} and shortens pauses.
This is only true to an extent: an important mutator cost is never traded for a small \textsc{gc} simplification.
However, the current balance tilts away from tagless \textsc{gc}s.

\paragraph{Reference Counting}

A variant of \textsc{gc} is \emph{reference counting} (\textsc{rc}): blocks of memory are prefixed with a counter that indicates the number of pointers (i.e., references) to that block.
\index{abbreviation!\textsc{rc}: reference counting, reference counter}
\index{reference counting}
\index{reference counter}
When a counter reaches zero its block is deallocated.
Consequently, the counters of blocks it pointed to are decremented, possibly leading to a cascade of deallocations.

Even though it is often considered to be a distinct approach to memory management, \textsc{rc} is merely a variant of \textsc{gc}.
Instead of scanning memory periodically to update the reachability graph and decide what can be safely deallocated, a program with \textsc{rc} updates the counters with each instruction and decides whether to deallocate values on the spot.
Bacon, Cheng and Rajan propose a framework of which \textsc{gc} and \textsc{rc} are both instances \cite{unified}.
In particular, they point out that optimisations of \textsc{gc}s (most notably, concurrent \textsc{gc}s) makes them closer to \textsc{rc}s.
Vice versa, optimisations to \textsc{rc}s, makes them closer to \textsc{gc}s.
In their framework, the main distinction between the two approaches is as follows: \textsc{rc}s explore and deallocate unreachable values (values are presumed useful until proven unreachable) whereas \textsc{gc}s scan reachable values and preserve them (values are presumed useless until proven reachable).
The authors explain this distinction using the concepts of \emph{matter} for the reachable-objects traced and saved by the \textsc{gc} and \emph{anti-matter} for the unreachable-objects explored and deallocated by the \textsc{rc}.
\index{matter}
\index{anti-matter}
We reuse this vocabulary in later Sections.

Note that \textsc{rc}s still have the same issue as other \textsc{gc}s: they restrict the representation of values.
Specifically, they require blocks to be prefixed with a counter and to include runtime type information for the cases when deallocations cascade.


\subsection{Linear and region regimes}
\label{sec:lin-reg}

\emph{Linear type systems} and \emph{region-based memory managers} are other families of approaches to memory management.
In these approaches, the source programming language is restricted by a type-like analysis – i.e., one that restricts the set of valid programs.
Both the linear and region regimes are restrictions that make it safe for the compiler to replace unreachability with another characterisation of uselessness.

\paragraph{Linear type systems}

In a linearly typed program, each value must be used exactly once.
\index{linear type system}
This constraint is enforced by a type-like analysis inspired by linear logic \cite{linlogic}.
Under this constraint, a value becomes useless right after it has been used, and can therefore be safely deallocated.
Note that in this case uselessness is akin to (but not exactly the same as) non-liveness – we explore this relation in Chapter~\ref{chap:further}.

Variants of linear type systems relax the value-usage restrictions.
\emph{Quasi-linear} type systems \cite{qlt} allow programmers to mix linearly and non-linearly typed values in a single program: the former are freed on use, the latter managed by a \textsc{gc}.
\index{linear type system!quasi-linear}
In an \emph{affine} type systems values can be used at most once, but are not required to be used.
\index{linear type system!affine}
Both variants relax the constraints of the linear regime, trading off some of its benefits for increased expressivity.

We illustrate the necessary program changes that (non-relaxed) linear types induce in Figure~\ref{fig:lin-ex}.
These changes constitute the administrative overhead programmers must go through to satisfy the type-like analysis.
They include explicitly copying \texttt{data} because it is used multiple times.
They also include explicitly ignoring \texttt{d4} because it would otherwise go unused in the \texttt{else} branch.
This administrative overhead highlights a practical penalty as well as a conceptual one: programmers are once again involved in memory management.
Taking this conceptual penalty to the extreme, one can interpret the \texttt{ignore} function like C's \texttt{free} and see linear types as compiler-checked, manual memory management.

\begin{figure}
\begin{subfigure}[b]{0.45\textwidth}
\begin{alltt}\small
let rec sum = function
  | [] -> 0
  | x::xs -> x + sum xs
;;
let rec len = function
  | [] -> 0
  | \_::xs -> 1 + len xs
;;
let rec get () =
  (*read from stdin and parse*)
  let data = readIntegers () in


  if sum data / len data > 1 then
    data
  else

    get ()
;;
\end{alltt}
\caption{Conventional OCaml code}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.45\textwidth}
\begin{alltt}\small
let rec sum = function
  | [] -> 0
  | x::xs -> x + sum xs
;;
let rec len = function
  | [] -> 0
  | \_::xs -> 1 + len xs
;;
let rec get () =
  (*read from stdin and parse*)
  let data = readIntegers () in
  \textcolor{green}{let (d1,d2) = deepCopy data in}
  \textcolor{green}{let (d3,d4) = deepCopy d1 in}
  if sum \textcolor{blue}{d2} / len \textcolor{blue}{d3} > 1 then
    \textcolor{blue}{d4}
  else
    \textcolor{green}{ignore d4;}
    get ()
;;
\end{alltt}
\caption{Linear OCaml code}
\end{subfigure}
\caption{Before and after: administrative overhead of linear regimes}
\label{fig:lin-ex}
\end{figure}

\paragraph{Region-based memory management}

With a region-based memory manager \cite{rbmm}, values are allocated in a \emph{region}.
\index{region-based memory manager}
\index{region}
Additionally, a type-like analysis enforces that values do not escape their region.
I.e., under the region regimes, pointer from an outer into an inner region are prohibited.
When a region falls out of scope, all its values are deallocated.

We illustrate the administrative overhead that a region regime induce in Figure~\ref{fig:reg-ex}.
Because the \texttt{altzip} mixes together the elements from both of its argument, it imposes they are allocated in the same region.
Consequently, the elements of \texttt{us} and \texttt{vs} (both passed to \texttt{altzip}) must be allocated in the same region.
As a result, their life-time is tied: elements of \texttt{vs} cannot be deallocated before the elements of \texttt{us}.
Unfortunately, the \texttt{us} is returned from \texttt{f}: its memory can be arbitrarily long-lived (depending on the caller).
Thus, the memory of \texttt{vs} is kept for an arbitrary long time even though it is not useful.
This is fixed in Figure~\ref{fig:reg-ex-fixed}: \texttt{us} is copied; one replica entangles inconsequently with \texttt{vs}; the other replica is returned.
Note that these changes are not necessary for the program to run; but they reduce memory footprint of the program.

Whilst both systems are safe and efficient, they limit expressiveness.
Indeed, programmers must express their ideas within a (purposefully) limiting framework.
In some cases, programmers are required to add annotations or even memory management instructions (such as copying) to their program.
This administrative overhead highlights the same conceptual penalty as for linear types: programmers are once again involved in memory management.

\vspace{1.5ex}

Regions (in conjunction with other techniques) are used in Cyclone \cite{cyclone} to manage memory safely.
A mix of linear types and regions is used in Rust \cite{rust}.

\begin{figure}
\begin{subfigure}[b]{0.45\textwidth}
\begin{alltt}\small
let rec altzip = function
  | x::xs, y::ys ->
    (x,y)::altzip (ys,xs)
  | _ -> []
;;
let f () =
  let us = readData () in
  let vs = readData () in
  (*compute*)

  let ws = altzip (us, vs) in
  use (ws);
  us
;;
\end{alltt}
\caption{Conventional OCaml code}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.49\textwidth}
\begin{alltt}\small
let rec altzip = function
  | x::xs, y::ys ->
    (x,y)::altzip (ys,xs)
  | _ -> []
;;
let f () =
  let us = readData () in
  let vs = readData () in
  (*compute*)
  \textcolor{green}{let (us1, us2) = deepCopy us in}
  let ws = altzip (\textcolor{blue}{us1}, vs) in
  use (ws);
  \textcolor{blue}{us2}
;;
\end{alltt}
\caption{Region-friendly OCaml code}
\label{fig:reg-ex-fixed}
\end{subfigure}
\caption{Before and after: administrative overhead of region regimes}
\label{fig:reg-ex}
\end{figure}

\subsection{Memory re-use}

Another area of interest is \emph{memory re-use} – or simply \emph{re-use}.
\index{memory re-use}
\index{re-use}
A re-use system endeavours to replace allocations by mutations of dead memory blocks.
That is, when a re-use system is successful, the program avoids the inefficient sequence: deallocate a dead block, allocate a new block, initialise the freshly allocated block.
Instead, the program simply mutates the dead block to represent the new value.
In order to decide re-use it is necessary to detect dead blocks and to find subsequent allocations for blocks of the same size.

Manual re-use is possible in programming languages that feature mutation.

Automatic re-use is always used in conjunction with another system – such as a \textsc{gc} \cite{mercury}, or a linear-type system \cite{linearreuse}.
The re-use system decreases the rate of allocations which reduces the workload of the memory management system.



\section{System programming}

System programming consists of writing and maintaining software in the lower layers of the operating system: hypervisors, device drivers, network stacks, file systems, cryptographic libraries, etc.
\index{system programming}

Memory management in system programs is an important topic.
The \textsc{minix} author, Andrew Tanenbaum, writes \cite{minix}:
\begin{quote}
\addfontfeature{Ligatures=Discretionary}
\addfontfeature{Contextuals=Alternate}
\addfontfeature{StylisticSet=1}
\addfontfeature{Style=Alternate}
	My initial decision back in 1984 to […] avoid dynamic memory allocation (such as malloc) and a heap in the kernel […] avoids problems that occur with dynamic storage management (such as memory leaks and buffer overruns).
\end{quote}

These specific problems are common in many programs written in C.
Despite this drawback, C is the most common language used for system programming.
As a result, serious memory management bugs are continuously discovered in system software.

There are historical reasons for C's prevalence: Unix and all its successors were written in C.
As a result, there is, to this day, a cultural preference for C amongst systems programmers.

However, there is another reason for C to persist as the leading language for system programming.
Indeed, consider the specificity of system software: it interacts directly with hardware.
As a result, system software handles values in native format – e.g., ethernet frames as they appear on the wire.
For this reason, programmers need full control over the representation of values in memory.
This was pointed out by the FoxNet project \cite{foxnet}: a full network stack in \textsc{sml} suffering from efficiency issues due to the repeated copies and conversions back and forth between the system's native representation and the \textsc{gc}-compatible layout.

\textsc{Gc}s, \textsc{rc}s and region regimes prevent the programmers from controlling the layout of values in memory.
As such, they are not compatible with the memory management requirements of system programming.
Conservative \textsc{gc}s are leaky, which is problematic for long-lived programs such as servers and daemons.
They too are ill-suited for system programming.
Linear type systems are restrictive: the complete opposite of C.
Their adoption faces the inertia of the cultural prevalence of C in system programming.

A sound option for system programming is Rust – which was developed for that specific purpose.
Rust can be used with a \textsc{gc}, but, by default, manages the memory through a hybrid linear-region regime that does not restrict the value layout.
Rust's hybrid linear-region regime is inspired by manual memory management guidelines widespread amongst C programmers.
Thus, Rust's constraints are looser than linear or region regimes and they broadly follow existing customs of system programmers.
We explore Rust in more details in Chapter~\ref{chap:design-space}, and again in Chapter~\ref{chap:further}.


\section{\textsc{Asap}}

The rest of this dissertation presents a novel approach to memory management: \textsc{a}s \textsc{s}tatic \textsc{a}s \textsc{p}ossible (\textsc{asap}).
\textsc{Asap} is fully automatic: programmers are oblivious to the memory management just like with a \textsc{gc}.
\textsc{Asap} is agnostic of the memory representation which can be taken care of by the programmers when necessary and left to the compiler otherwise.
To provide these properties, \textsc{asap} analyses the program to detect value usage (at which points of the program are which values accessed) and aliasing (in what way which values alias).
Based on this information, \textsc{asap} inserts instructions within the program to deallocate memory when appropriate.
For the cases when it is not possible to decide at compile time if the memory representing a value can be safely reclaimed, \textsc{asap} inserts specialised code that will determine safety during execution.
Interestingly, because the type of values is known when the code is generated, it is able to scan and deallocate values without runtime types nor tags.


\section{Plan}

We first give some technical background in Chapter~\ref{chap:prerequisites}.
Specifically, we introduce intermediate representations and three-value logic and give an overview of data-flow analysis techniques.

We then explore the design space of memory management strategies in\linebreak[4] Chapter~\ref{chap:design-space}.
We establish a lexicon specialised for memory management.
This new-found vocabulary allows us to describe existing memory management strategies in a novel way and reveal a few unexplored areas in their design space.
One of these lacunae, the static-automatic gap, we explore further.

We introduce \emph{paths} in Chapter~\ref{chap:paths}, a compile-time abstraction of the heap.
In Chapter~\ref{chap:asap}, we present \textsc{asap}:  a novel memory-management strategy that fits in the static-automatic gap.
We then extend \textsc{asap} to support more programming-language constructs in Chapter~\ref{chap:extensions}.
We present our prototype implementation of \textsc{asap} in Chapter~\ref{sec:implementation}.

We compare \textsc{asap} to existing strategies in Chapter~\ref{chap:comparison}.
In this comparison, we also show ways in which \textsc{asap} can be combined with other strategies, providing several hybrid solutions to memory management.
We take a specific look at linear and region regimes in Chapter~\ref{chap:subsumptions}.
Finally, we explore insights gained during the creation of \textsc{asap}.


