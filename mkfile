
LATEX=xelatex
BIBTEX=bibtex

CHAPTERS= \
	c00a-title.tex \
	c00b-guard.tex \
	c01-intro.tex \
	c02-preliminaries.tex \
	c03-design-space.tex \
	c04-Paths.tex \
	c05-ASAP.tex \
	c06-extensions.tex \
	c07-implementation.tex \
	c08-comparisons.tex \
	c09-subsumption.tex \
	c10-further.tex \
	c98-conclusion.tex \
	c99-guard.tex

GLUE= \
	glue-packages.tex \
	glue-wrapper.tex

STANDALONES= ${CHAPTERS:%.tex=%.standalone.tex}

PANDOCOPTS= \
	--tab-stop=4

TARGETS=dissertation.pdf abstract-and-title.pdf

all:V: $TARGETS

standolones:V: ${STANDALONES:%.tex=%.pdf}

%.standalone.tex: %.tex
	sed 's/INCLUDE/\\input{'$prereq'}/' <glue-wrapper.tex >$target

%.pdf: %.tex
	$LATEX $stem.tex

%.bbl: %.bib
	$BIBTEX $stem

dissertation.pdf: $GLUE $CHAPTERS dissertation.bbl dissertation.tex
	$LATEX dissertation.tex

abstract-and-title.pdf: $GLUE abstract-and-title.tex
	$LATEX abstract-and-title.tex


publish:V: $TARGETS
	scp dissertation.pdf ramsey:public/drafts
	scp abstract-and-title.pdf ramsey:public/drafts

fix:V:
	$LATEX dissertation.tex
	$BIBTEX dissertation
	$LATEX dissertation.tex
	$LATEX dissertation.tex

clean:V:
	rm -f *.aux *.log *.out *.blg *.bbl
	rm -f *.standalone.*
	rm -f $TARGETS
