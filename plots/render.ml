open Data

let key size height =
	let x = 50 + size * 100 - 100 in
	let y = height - 100 + 25 + 5 in
	Printf.sprintf
		"<text x='%d' y='%d' font-size='12'>%d</text>"
		x y size
;;

let offset v = int_of_float (v *. 100.);;

let mean size value height =
	let x = 50 + size * 100 - 100 in
	let y = height - 100 - (offset value) + 5 in
	Printf.sprintf
		"<text x='%d' y='%d' font-size='10px'>%.2f</text>"
		x y value
;;

let line size bot top height =
	let x = 50 + size * 100 - 100 in
	let yb = height - 100 - (offset bot) in
	let yt = height - 100 - (offset top) in
	Printf.sprintf
		"<line x1='%d' y1='%d' x2='%d' y2='%d' stroke-width='1' stroke='black'/>"
		x yb x yt
;;

let rr size datum height =
	let key = key size height in
	let mean = mean size datum.mean height in
	let top = line size datum.third_quart datum.maximum height in
	let bot = line size datum.minimum datum.first_quart height in
	Printf.sprintf "%s\n%s\n%s\n%s" key mean top bot
;;

let axis height =
	let y = height - 100 in
	Printf.sprintf
		"<line x1='0' y1='%d' x2='700' y2='%d' stroke-width='1' stroke='gray'/>"
		y y
;;

let r data =
	let max_value = List.fold_left max 0. (all_valss data) in
	let height = offset max_value + 150 in
	let intro = "<?xml version='1.0' encoding='UTF-8' ?>" in
	let opening =
		Printf.sprintf "<svg width='700' height='%d' viewbox='0 0 700 %d' xmlns='http://www.w3.org/2000/svg' version='1.1'>"
			height height
	in
	let closing = "</svg>" in
	let axis = axis height in
	let bulk =
		String.concat "\n"
			(List.map (fun (size, datum) -> rr size datum height) data)
	in
	Printf.sprintf "%s\n%s\n%s\n%s\n%s"
		intro
		opening
		axis
		bulk
		closing
;;

let () =
	let oc = open_out "flat_shape.svg" in
	output_string oc (r flat_shape);
	close_out oc;
	let oc = open_out "flat_share.svg" in
	output_string oc (r flat_share);
	close_out oc;
	let oc = open_out "flat_access.svg" in
	output_string oc (r flat_access);
	close_out oc;
	let oc = open_out "deep_shape.svg" in
	output_string oc (r deep_shape);
	close_out oc;
	let oc = open_out "deep_share.svg" in
	output_string oc (r deep_share);
	close_out oc;
	let oc = open_out "deep_access.svg" in
	output_string oc (r deep_access);
	close_out oc;
	()
