\chapter{\textsc{Asap} in relation to other strategies}
\label{chap:comparison}

We now take a look at \textsc{asap} in relation with the other approaches listed in Chapter~\ref{chap:design-space}.
We focus on fitness for system programming.

\section{Comparison with existing strategies}

Here we compare \textsc{asap} to other approaches.
We also explore ways in which to combine \textsc{asap} with these other approaches.

\subsection{Manual memory management \textit{à la} C}

\textsc{Asap} and the manual approach of C have little in common from the programmer's perspective: \textsc{asap} abstracts the memory entirely from the programmer whilst C leaves the memory explicit.
Despite this major difference, they share some common ground.
Specifically, both strategies provide control over the memory representation of values.
This is achieved, in both cases, by avoiding to rely on the runtime.

Note that \textsc{asap} does not let the programmer choose the deallocation points (unlike C).
Nevertheless, the programmer can, after compilation, inspect the emitted code to see what deallocation points have been chosen by \textsc{asap}.
For ease of reading, \textsc{asap} can be made to dump the intermediate representation with calls to $\mathit{CLEAN}$ before their expansion into scanning code.
This facility can help predict behaviour during execution.

Where C outshines \textsc{asap} is in the ability it gives programmer to optimise memory management.
Specifically, the programmer can decide to wait until all known aliases of a value are dead before deallocating it.
By contrast, \textsc{asap} will generate scanning code that is run during execution.
Where \textsc{asap} outshines C is safety: C has none.
This is important for system programs because they underpin all other software.

We posit it is possible to use \textsc{asap} to check human-specified deallocations are correct.
In such an approach, the programmer has access to the $\mathit{CLEAN}$ compile-time function and decides when to deallocate what.
Moreover, unlike with $\texttt{free}$, the programmer can specify a matter-set: values to keep.
The role of \textsc{asap} is then reduced to checking correctness and expanding calls to $\mathit{CLEAN}$.
To ensure a program is leak-free, \textsc{asap} must enforce that all zones are deallocated before they fall out of scope.
Additionally, to ensure a program does not contain use-after-free, \textsc{asap} must enforce several properties.
First, only zones with $\mathit{Access}$ $0$ ever appear in the anti-matter set.
Second, when a zone $z$ is deallocated at point $\circled{\pi}$, any zone $z'$ such that $\mathit{Shape}(\circled{\pi})(z,z') \geq \top$ must appear in either the matter-set or the anti-matter set.
This last point ensures that overlap between zones is never overlooked.
Additional properties might be necessary; further investigation is left as future work.

Note that integrating \textsc{asap} with C specifically is more complicated than just checking for the properties above.
Indeed, C's type system is difficult to translate into µL's.
Additionally, \textsc{asap} assumes that programs are well-typed which is not guaranteed, even in the middle-end of C compilers.
However, the use of compiler-checked $\mathit{CLEAN}$ could be applied to a different C-like language, providing a sound basis for a system-programming-friendly memory management.


\subsection{\textsc{gc}}

\textsc{Asap} is suited as a drop-in replacement for \textsc{gc}s because, from the point of view of the programmer, they are similar: memory management is abstracted away in a safe and correct way.
However, the execution of programs under \textsc{asap} differs significantly: \textsc{gc}s batch memory management operations in big and asynchronous collection cycles whilst \textsc{asap} performs small and synchronous sets of operations.
As a result, the relative efficiency of these two strategies can vary with different program patterns.
In particular, consider programs that are short lived or do not use a lot of memory: under a \textsc{gc} the program might exit before a collection cycle ever happens, in which case memory deallocation uses absolutely no computing resources.
(Note however, that allocations are different under \textsc{gc}ed and non-\textsc{gc}ed languages.
Thus, some resources might be used during allocations to initialise \textsc{gc} marking fields.)

One of the way to characterise \textsc{asap} is as an inlined \textsc{gc} which is statically optimised.
Specifically, \textsc{asap} zealously runs \textsc{gc}-like cycles at every program point but only from a limited set of roots and along a pruned set of paths.
The set of roots and paths form zones which are chosen statically based on information inferred by \textsc{asap}'s analyses.
The zone specialisation is such that, at some program points, the “inlined \textsc{gc} call” is removed altogether.
However, this characterisation is imprecise in three major ways.
First, the timeliness of the two approaches differs: the approximation of waste by $\mathit{Access}$ is more precise than the characterisation by unreachability of \textsc{gc}s.
Second, the collection points are disjoint: in \textsc{asap} collections appear after each binding and match\footnote{Remember that when it is statically decidable that a specific collection will free no memory at all, it is removed altogether.} instead of allocation points.
Third, in \textsc{asap}, the scanning operations do not rely on runtime type information (such as block size and pointerness).

Another important difference with \textsc{gc}s is the relation with the underlying OS.
Indeed, runtimes that include \textsc{gc}s request a chunk of memory from the OS and manage it for the program.
%are there exceptions to this?
When the programmer allocates a value (either explicitly such as with Java's \texttt{new} or implicitly as with constructors in ML), the runtime reserves some of the pre-allocated chunk for that value.
By contrast, under \textsc{asap}, the memory can be reserved and released using \texttt{malloc} and \texttt{free}, just like in C.
This latter style also reserves memory, albeit in smaller chunks.
Under \textsc{asap}, the memory management primitives can be specialised to support either scheme.

\subsection{\textsc{Asap}-\textsc{gc} hybrid as a liveness-assisted \textsc{gc}}

An interesting hybrid is to use \textsc{asap} for marking with a more standard \textsc{gc}-like sweeper.
Compared to \textsc{asap}, this hybrid requires runtime type information – unless a tagless \textsc{gc} is used for sweeping.
%A more favourable aspect is compaction – an operation of \textsc{gc}s whereby memory blocks are grouped in the same area of the heap.
%Compaction has the benefit of grouping the free cells of the heap together, making allocations simpler and faster and grouping the allocated blocks together increasing spatial locality.
Compared to \textsc{gc}, the hybrid approach is timelier: waste is approximated by $\mathit{Access}$ (essentially liveness) which is more precise than reachability.
In this respect, the hybrid behaves like a liveness-assisted \textsc{gc}.

The \textsc{asap}-\textsc{gc} hybrid has timeliness similar to \textsc{lagc}'s (because both use a similar waste approximation) but they differ in other aspects.
The main difference is that \textsc{lagc}s characterise pointers as dead and prevent the \textsc{gc} from following them (either by setting them to null or by communicating a path set to the \textsc{gc}).
\textsc{Asap}, on the other hand, characterises memory blocks as dead and synthesises code that, in the hybrid presented above, marks them as garbage.
As a result, \textsc{lagc} adds null pointers in the memory of the program (when it knows this pointer will not be followed) whilst \textsc{asap} deallocates blocks which creates dangling pointers.



\subsection{\textsc{rc}}

Reference counting, like \textsc{asap}, is a safe, synchronous memory management strategy.
However, \textsc{rc} approximates waste by unreachability which is less timely than \textsc{asap}'s approximation by $\mathit{Access}$.

\textsc{Rc}, like \textsc{asap}, is a synchronous strategy: deallocation points are known, and thus static information (such as type) is available.
Thus, the cascading deallocation code can be generated at compile time.
Specifically, a $\mathit{SCAN}$-like compile-time function can be used.
This $\mathit{SCAN}$-like function integrates tests to check the value of the reference counters.

Using a $\mathit{SCAN}$-like function gives opportunity for optimisations.
Specifically, if the compiler is able to statically determine that a reference counter, or a set thereof, is $0$, it can specialise the generated code to perform unconditional deallocations.


\subsection{Re-use}

Re-use systems, such as Mercury's \textsc{ctgc}, reduce the workload of a memory management strategies by combining deallocation-allocation sequences into simple mutations.

There are two ways to combine \textsc{asap} with a re-use system.
The first possibility is to start with the re-use transformation and follow up with \textsc{asap}.
In this situation, \textsc{asap} receives an optimised program in which some blocks are re-used.
Note that, in the optimised program, some values get mutated.
As was noted in Chapter~\ref{chap:extensions}, mutation can be handled by \textsc{asap} with the following caveats.
At the program point where the mutation happens, a call to $\mathit{CLEAN}$ is always inserted.
This call might be optimised away, depending on the statically inferred information.
Mutation of non-local values (specifically, of arguments that are used by the caller after return) incurs runtime costs.
Thus we posit that limiting re-use to local values is preferable.

The second possibility is to start with \textsc{asap} and follow up with the re-use transformation.
In this situation, the re-use system's work is simplified: it can see explicit deallocations instead of having to guess non-liveness of values.
Thus, the re-use system merely needs to find pairs of deallocations and allocations close together and transforms them into a mutation.
This approach is simpler: \textsc{asap} is not involved, and the re-use system only detects deallocation/construction pairs.
Note however, that only unconditional deallocations are ever considered for re-use.


\subsection{Regions}

Region systems share many features with \textsc{asap} such as synchrony, agnosticism to memory representation, and correctness.
Comparing the means by which regions and \textsc{asap} provide these features is interesting.
The following comparison assumes Tofte-Talpin style of regions \cite{rbmm-tools} inferred by the compiler.

To infer regions, the compiler first assigns a region variable to each program variable.
Then, the compiler emits constraints on region variables based on instructions of the program.
E.g., if a variable located in region $\rho_1$ is stored inside a list located in region $\rho_2$, then $\rho_1$ must be an outer region and $\rho_2$ an inner region – or, in other words, $\rho_1$ must be deallocated after $\rho_2$.
These constraints form a partial order over the region variables.
Finally, the compiler solves these constraints to find which region variables are associated to the same region.
This is achieved by propagating the partial order by transitivity and detecting equality by antisymmetry.
The result is a coarse description of the heap: which value is stored in which region and in what directions are inter-region pointers allowed.

This bears resemblance with the way \textsc{asap} infers information about the heap.
Indeed, \textsc{asap} too collects constraints based on the program instructions.
These constraints are also based on instructions that affect points-to relationship between values in the program.
Unlike regions however, \textsc{asap} solves these constraints by inferring fine-grain information about the shape of the heap: in what way may a value point to another value.

This leads to an interpretation of regions as a lossy simplification of heap shape analysis.
With regions, all that remains of the description of the heap is a specific partitioning with a partial order of the partitions. 
Because region inference sets out to produce this coarse description, it is simpler than \textsc{asap}'s Shape and Share analyses.
On the other hand, region systems describe the heap less precisely and are consequently less timely than \textsc{asap}.


\subsection{Necessity analysis}

In \cite{necessity}, G. W. Hamilton and Simon B. Jones describe necessity based garbage collection (\textsc{nbgc}): an analysis and subsequent code transformation to assist with memory management.
Specifically, a necessity analysis is performed in order to decide, for each program point, what part of each value is needed later.
Equipped with this information, the code is transformed to deallocate unneeded values earlier than a \textsc{gc} would allow.
Additionally, when possible, the code is transformed so that would-be deallocated cells are re-used.

Thus, \textsc{nbgc} is similar to the working of \textsc{asap}.
Specifically, \textsc{nbgc} describes the necessity of a value by a tree.
These trees follow the grammar $P ::= 0 | 1 | (\{0 | 1\} \times P \times P)$ where
$0$ represents a value that is not needed,
$1$ represents a value that may be needed, and
$(r,p_h,p_t)$ represent a list with $r$ the necessity of the root cons cell, $p_h$ the necessity of the head, and $p_t$ the necessity of the tail.

There are two important differences between \textsc{asap} and \textsc{nbgc}.
First, necessity trees are less precise than paths.
Indeed, it is impossible to use trees to express such statements as “for all the elements of a list $l$, the $\mathit{Left}$ fields are needed, but the $\mathit{Right}$ fields are not.”
This is impossible because of the absence of the repetition operator in trees.
The statement is formalised in paths using the following equations.

\begin{displaymath}
\begin{array}{r c l}
\mathit{Access}(l, \mathit{elems} \cdot \mathit{Left}) & = & 1 \\
\mathit{Access}(l, \mathit{elems} \cdot \mathit{Right}) & = & 0 \\
\end{array}\\
\text{where } \mathit{elems} = (\mathit{Cons} \cdot \mathit{Tail})^{*} \cdot \mathit{Cons} \cdot \mathit{Head}
\end{displaymath}

Second, when the necessity of a value cannot be decided, \textsc{nbgc} falls back to a standard \textsc{gc}.
That is \textsc{nbgc} assists the \textsc{gc} by introducing re-use and early deallocations.
By contrast, when the Access property of a value cannot be decided, \textsc{asap} emits code that determines the safety of the deallocation during execution.
That is \textsc{asap} replaces the \textsc{gc} by emitting complete deallocation code.


\subsection{Individual object deallocation}

In \cite{individual-objects-free}, Cherem and Rugina present a method for compile-time deallocation of individual objects.
Their approach relies on an analysis and a code transformation.
The analysis statically determines the number of references to each individual objects.
To avoid the analysis diverging, the count is bounded to a constant $k$, after which the value $\inf$ approximates the reference count.
When a reference count falls to $0$, the code transformation inserts a \texttt{free} statement.
This statement may change the reference count of other objects; this is dealt with by fixpointing the analysis and the code transformation until no more \texttt{free} statements are inserted.

Experimental results from the authors indicate that the fixpoint is generally reached within three iterations.
They also show a execution time low overhead.
Finally, an interesting measure is that, depending on the program, the approach frees between 0\% and 90\% of objects.
To accommodate for the remainder of the objects, the authors propose to complement their approach with a \textsc{gc}.

Thus, this approach differs from \textsc{asap} in multiple ways.
First, the aim differs in that the approach does not intend to be complete: it lightens the workload of the \textsc{gc} rather than replacing it.
Second, the means differ in that the approach uses analyses that statically approximate reference counting instead of approximating the heap shape.
The means also differ in that the approach generates simple free instructions for individual objects instead of deallocation code that descends through a data structure.




\section{Comparison with Rust and C}

We use the same table as in Chapter~\ref{chap:design-space} to compare \textsc{asap} with two languages that are used in system programming: Rust and C.
The result is detailed in Figure~\ref{fig:table-design-space-asap}.
Note that \textsc{asap} offers features similar to C's with the addition of safety.

\begin{sidewaysfigure}
	\begin{tabular}{@{}lccccccc@{}}\toprule
		Strategy        & Agnostic(\thumbsup) & Correct(\thumbsup) & Restrictive(\thumbsdown) & Synchronous & Approximative & Re-use(\thumbsup) \\
		\midrule
		\textit{À la} C & \cmark              &                    &                          & \cmark      & N/A           & \cmark (a)        \\
		Rust            & \cmark              & \cmark             & \cmark                   & \cmark      & \cmark (b)    &                   \\
		\textsc{asap}   & \cmark              & \cmark             & (c)                      & \cmark      & \cmark (d)    &                   \\
		\bottomrule
	\end{tabular}
	\begin{enumerate}[noitemsep, label=(\alph*)]
		\item manual re-use
		\item liveness (aided by ownership annotations)
		\item assumes the program is type correct
		\item liveness (inferred by compiler)
	\end{enumerate}
\caption{\textsc{Asap} and other system-programming-friendly strategies}
\label{fig:table-design-space-asap}
\end{sidewaysfigure}

