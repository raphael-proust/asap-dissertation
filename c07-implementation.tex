\chapter{Implementation}
\label{sec:implementation}
\label{chap:implementation}

We developed a prototype of \textsc{asap}.
We now briefly describe its implementation, discuss its limitations, show an example run, and present efficiency measurement.

\section{Code overview}

The prototype is written in OCaml and consists of 2250 lines of code including support modules for \textsc{3vl}, paths, and zones.
An additional 8500 lines of code provided a front-end (using three intermediate representations), a back-end (using an evaluator), pretty printing (for dumping inferred information and intermediate representations), as well as several support modules (for managing environments, unique identifiers, and maps).

\paragraph{Front-end}

The lexer and parser are automatically generated using \texttt{ocamllex} and \texttt{menhir}\footnote{\texttt{ocamllex} is distributed with OCaml, \texttt{menhir} is available at \url{http://gallium.inria.fr/~fpottier/menhir/}.}.
They produce an \textsc{ast} which is transformed into dL (pronounced “deci-language”), into cL (“centi-language”), into mL (“milli-language”), into µL.
Together, these compilation passes provide the syntactic sugar of Chapter~\ref{chap:prerequisites} as well as the following abstractions: nested functions, nested patterns, nested constructors and nested bindings.

\paragraph{Analyses}

The prototype analyses all of the three properties: $\mathit{Shape}$, $\mathit{Share}$, $\mathit{Access}$.
It is also able to dump the results of the analyses (as demonstrated below).

\paragraph{Transformation}

Our prototype inserts calls to $\mathit{CLEAN}$ within the analysed code.
However, these calls are not currently expanded into the scanning code using the staging approach of Chapter~\ref{chap:asap}.
Instead they are interpreted by our back-end.

\paragraph{Back-end}

The evaluator recursively traverses the program in its final intermediate representation (i.e., after it is analysed and transformed).
It emulates memory by maintaining a map from addresses (represented as integers) to memory blocks (represented as native OCaml values).
This map is implemented naïvely as a list.


\section{Prototype limitations}

The implementation is a prototype with various limitations not arising from the theory.

\paragraph{No calls from a recursive function to a recursive function}

The prototype does not support programs in which a recursive function makes a call to another recursive function.
This is caused by our implementation of paths: in the prototype, only paths of a limited form are allowed.
Specifically, the prototype only handles sequences of possibly repeating alternatives of sequences.
This restricted form of path is not powerful enough to express the heap properties that appear when recursive functions call recursive functions.
Specifically, the prototype cannot handle nested repetitions in paths.

In order to support these programs, the prototype's path library would need to be improved to handle the full range of paths described in Chapter~\ref{chap:paths} rather than the limiting subset it currently handles.


\paragraph{Nested matching bug}

Nested patterns are compiled as nested matching operations.
However, because of the naïve implementation of the transformation, backtracking from one level to the previous one is not supported.
As a result, it is easy to write programs that fail during evaluation due to matching errors.

E.g., the program in Figure~\ref{fig:matching-bug} fails at evaluation time because the matching engine commits to the first branch (upon matching the top \texttt{Cons} node) and is unable to backtrack (upon failing to match the \texttt{Tail} field).


\begin{figure}
\begin{alltt}\small
type cons = \{ Head:word ; Tail:list \}
and nil = \{ \}
and list =
  | Cons of cons
  | Nil of nil

letrec main (): word =
  let x : int =
    match Cons \{Head=0; Tail=Nil \{\} \} with
      [ Cons \{Tail=Cons _\} -> 0
      | _ -> 1
      ]
  in
  x
;;
\end{alltt}
\caption{Example program triggering the matching bug in the prototype}
\label{fig:matching-bug}
\end{figure}


\paragraph{Performance}

No effort was made towards any form of acceptable performance in the prototype.
In particular, \textsc{3vl} sets and relations are represented by triplets of lists, one containing values that map to $0$, one $\top$ and one $1$.
Some operations such as transitive closure of aliasing information are particularly inefficient.

The evaluator is implemented as a recursive function traversing the final representation.
During this evaluation, an abstract representation of the stack and heap is passed around.
This is also inefficient.


\section{Example}

We show the result of the different analyses on a sample program, as inferred by the prototype.
The source program, written in the front-end syntax with nested patterns and constructors, is shown in Figure~\ref{fig:prototype-run-source}.
Other syntax difference include: $\texttt{letrec}$ instead of $\texttt{fun}$ and double semi-colon ($\texttt{;;}$) to terminate function definitions.


The result of the analyses is presented in Figure~\ref{fig:prototype-run-analyses}.
Note, first, that the syntactic sugar of the source program has been compiled down – e.g., the nested pattern is replaced by a nested match with the fresh variable \texttt{x1}.
They are presented as printed by the prototype with additional line breaks for readability.
The decorations are presented as triplets $\texttt{<}\: \mathit{Shape}\: \texttt{\%}\: \mathit{Share}\: \texttt{\%}\: \mathit{Access}\: \texttt{>}$ and they read as follows.
\begin{itemize}
	\item[\texttt{1}] The \textsc{3vl}-certainty of $\mathit{Access}$ for both $(\mathit{ret}, \epsilon)$ (rendered as \texttt{\_ret.}) and $(\texttt{l}, \epsilon)$ (rendered as \texttt{l.}) is $1$.

		The \textsc{3vl}-certainty of $\mathit{Access}$ for the other zones is $\top$ as indicated by the question marks.
		E.g., \texttt{?l.Cons.Head} indicates the uncertainty that the zone $(\texttt{l}, \texttt{Cons} \cdot \texttt{Head})$ is accessed.

		Note that, for brevity, the prototype does not print reflexive components of $\mathit{Shape}$ (such as \texttt{l.=l.}) at this point of the program.
		However, this is not the case for all reflexive components at all program points (see below).

	\item[\texttt{3}] The $\mathit{Shape}$ decoration indicates that the wildcard pattern (treated as a variable by our prototype) corresponds to the \texttt{Nil} component of the list \texttt{l} (rendered as \texttt{\_.=l.Nil}).
		The decoration also includes some reflexive components.
	\item[\texttt{7}] The $\mathit{Shape}$ decoration indicates here that the return value (\texttt{\_ret}) aliases with the value \texttt{x0}.
	\item[\texttt{10}] The $\mathit{Shape}$ decoration indicates that, in this branch, the variable \texttt{x1} is bound to the cons cell of \texttt{l} (rendered as \texttt{l.Cons=x1.}).
	\item[\texttt{11}] The $\mathit{Access}$ decoration indicates that \texttt{l}'s cons cell is further explored.
		Specifically, the \texttt{l1.Cons.Head} appears in the decoration.
	\item[\texttt{14}–\texttt{16}] The $\mathit{Shape}$ decoration indicates the existence and structure of the aliasing between \texttt{l}, \texttt{x1} and \texttt{h}.
	\item[\texttt{17}] This $\mathit{Access}$ decoration is particularly interesting.
		Notice how, it immediately precedes a return construct and it only mentions the returned value (through its different aliases).
		This ensures the memory is not deallocated before it is returned to the caller.
	\item[\texttt{19}–\texttt{24}] This $\mathit{Shape}$ decoration is similar to the preceding one (lines \texttt{14}–\texttt{16}) with additional mention of \texttt{\_ret}.
	\item[\texttt{26}–\texttt{29}] This $\mathit{Shape}$ decoration is similar to the preceding one (lines \texttt{19}–\texttt{24}) but removes mentions of \texttt{h}.
		This is because \texttt{h} has fallen out of scope at line \texttt{25}.
	\item[\texttt{31},\texttt{32}] This is the exit point of the function where both branches join together.
		The $\mathit{Shape}$ decoration is the result of merging the decorations at the end of the two branches.
		As a result, it maps some pairs of zones to $\top$ (rendered as \texttt{\_ret.?=l.Cons.Head} where \texttt{?=} indicates potential but uncertain aliasing).
\end{itemize}

\begin{figure}
\begin{alltt}{\internallinenumbers\color{gray}first(l) = \color{black}<  %    %  _ret. l. ?l.Nil ?l.Cons ?l.Cons.Head>
\color{gray}  match l with
\color{gray}    [ Nil(_) ->
\color{gray}      \color{black}<_.=_. _.=l.Nil l.Nil=_. l.Nil=l.Nil  %    %  _ret.>
\color{gray}      let x0 = 0 in \color{black}
\color{gray}      \color{black}<_.=_. _.=l.Nil l.Nil=_. l.Nil=l.Nil  %    %  _ret.>
\color{gray}      return x0
\color{gray}      \color{black}<_.=_. _.=l.Nil l.Nil=_. l.Nil=l.Nil
\color{gray}      \color{black} _ret.=_ret. _ret.=x0. x0.=_ret. x0.=x0.
\color{gray}      \color{black} %    %  >
\color{gray}    | Cons(x1) ->
\color{gray}      \color{black}<l.Cons=l.Cons l.Cons=x1. x1.=l.Cons x1.=x1.
\color{gray}      \color{black}%    %  _ret. l.Cons x1. l.Cons.Head x1.Head>
\color{gray}      match x1 with
\color{gray}        [ \{Head=h\} ->
\color{gray}          \color{black}<h.=h. h.=x1.Head l.Cons=l.Cons l.Cons=x1. l.Cons.Head=h.
\color{gray}          \color{black} l.Cons.Head=x1.Head x1.=l.Cons x1.=x1. x1.Head=h.
\color{gray}          \color{black} x1.Head=x1.Head
\color{gray}          \color{black} %    %  _ret. l.Cons.Head x1.Head h.>
\color{gray}          return h
\color{gray}          \color{black}<_ret.=_ret. _ret.=h. _ret.=l.Cons.Head _ret.=x1.Head
\color{gray}          \color{black} h.=_ret. h.=h. h.=l.Cons.Head h.=x1.Head l.Cons=l.Cons
\color{gray}          \color{black} l.Cons=x1. l.Cons.Head=_ret. l.Cons.Head=h.
\color{gray}          \color{black} l.Cons.Head=l.Cons.Head l.Cons.Head=x1.Head x1.=l.Cons
\color{gray}          \color{black} x1.=x1. x1.Head=_ret. x1.Head=h. x1.Head=l.Cons.Head
\color{gray}          \color{black} x1.Head=x1.Head  %    %  >
\color{gray}        ]
\color{gray}        \color{black}<_ret.=_ret. _ret.=l.Cons.Head _ret.=x1.Head l.Cons=l.Cons
\color{gray}        \color{black} l.Cons=x1. l.Cons.Head=_ret. l.Cons.Head=l.Cons.Head
\color{gray}        \color{black} l.Cons.Head=x1.Head x1.=l.Cons x1.=x1. x1.Head=_ret.
\color{gray}        \color{black} x1.Head=l.Cons.Head x1.Head=x1.Head  %    %  >
\color{gray}    ]
\color{gray}    \color{black}<_ret.=_ret. _ret.?=l.Cons.Head l.Cons=l.Cons l.Cons.Head?=_ret.
\color{gray}    \color{black} l.Cons.Head=l.Cons.Head l.Nil=l.Nil %    %  >
}
\end{alltt}
\caption{Analysed program (annotations are curated for readability)}
\label{fig:prototype-run-analyses}
\end{figure}

\begin{figure}
\begin{alltt}
(*type definitions*)
type cons = \{ Head:word ; Tail:list \}
and nil = \{ \}
and list =
  | Cons of cons
  | Nil of nil

(*a function definition*)
letrec first (l:list) : word =
  match l with
  [ Nil _ -> 0
  | Cons \{ Head = h \} -> h
  ]
;;
\end{alltt}
\caption{Source program (with front-end syntax)}
\label{fig:prototype-run-source}
\end{figure}


\section{Scalability}

As explained above, the performance of the prototype are poor: the implementation has not been optimised for performance, nor has there been any effort towards improving its efficiency.
Consequently, analysing performance in terms of time of execution would conflate the poor performance of the prototype's library with the performance of the algorithm in general.

We present below relative measurements of time of execution for two categories of programs.
The results are normalised for the average execution time of the smallest program of their category.

\subsection{Flat programs}

We programmatically generated programs as presented in Figure \ref{fig:flat-progs}.
We call this category of programs \emph{flat} because the depth of the call graph is constant: the main function calls each of the other functions once.

\begin{figure}
\begin{alltt}
type tuple = \{ Left: word; Right: word \}

letrec f\(\sb{1}\)(x\(\sb{1}\)1: word, x\(\sb{1}\)2: word): tuple =
  \{ Left=x12; Right=x11 \}
;;

letrec f\(\sb{2}\)(x\(\sb{2}\)1: word, x\(\sb{2}\)2: word): tuple =
  \{ Left=x22; Right=x21 \}
;;

(* and so on defining f\(\sb{3}\), f\(\sb{4}\), etc. *)

letrec main() : word =
  let t\(\sb{0}\): tuple = \{ Left=4; Right=7 \} in

  let t\(\sb{1}\): tuple = f1(t\(\sb{0}\).Left, t\(\sb{0}\).Right) in
  let t\(\sb{2}\): tuple = f2(t\(\sb{1}\).Left, t\(\sb{1}\).Right) in
  (* and so on calling f\(\sb{3}\), f\(\sb{4}\), etc. *)
  let t\(\sb{n}\): tuple = fn(t\(\sb{n-1}\).Left, t\(\sb{n-1}\).Right) in

  t\(\sb{n}\).Left + t\(\sb{n}\).Right
;;
\end{alltt}
\caption{Automatically generated flat programs}
\label{fig:flat-progs}
\end{figure}

The performance of each of the three analyses, normalised for flat programs where $n = 1$ is presented in the tables of Figure \ref{fig:flat-perf}.
Specifically, the execution time of all three analyses (Shape, Share, and Access) was measured 75 times for different sizes of program.
For each set of measurements, the average, minimum, maximum, first quartile and third quartile, are normalised to the average for the program of size one.


\begin{figure}
\centering

\begin{subfigure}[b]{1\textwidth}
	{\addfontfeatures{Numbers={Monospaced,Lining}}
	\begin{tabular}{@{}lD{.}{.}{5}D{.}{.}{4}D{.}{.}{4}D{.}{.}{3}D{.}{.}{3}D{.}{.}{2}@{}}\toprule
	   program size 	& \multicolumn{1}{c}{1}   	& \multicolumn{1}{c}{2}   	& \multicolumn{1}{c}{3}   	& \multicolumn{1}{c}{4}   	& \multicolumn{1}{c}{5}   	& \multicolumn{1}{c}{7}   	\\
		\midrule
		minimum	& 0.6703 	& 2.790 	& 8.111 	& 18.78 	& 34.86 	& 120.5 	\\
		1st quartile	& 0.7450 	& 3.002 	& 8.721 	& 20.10 	& 39.75 	& 122.4 	\\
		average	& 1.000 	& 3.493 	& 9.221 	& 21.21 	& 41.88 	& 129.5 	\\
		3rd quartile	& 1.191 	& 3.553 	& 9.435 	& 21.64 	& 43.69 	& 134.1 	\\
		maximum	& 1.536 	& 5.989 	& 15.12 	& 36.09 	& 55.24 	& 141.2 	\\
		\bottomrule
	\end{tabular}
	}
\caption{Shape analysis}
\end{subfigure}

\vspace{1cm}

\begin{subfigure}[b]{1\textwidth}
	{\addfontfeatures{Numbers={Monospaced,Lining}}
	\begin{tabular}{@{}lD{.}{.}{5}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}@{}}\toprule
	   program size 	& \multicolumn{1}{c}{1}   	& \multicolumn{1}{c}{2}   	& \multicolumn{1}{c}{3}   	& \multicolumn{1}{c}{4}   	& \multicolumn{1}{c}{5}   	& \multicolumn{1}{c}{7}   	\\
		\midrule
		minimum	& 0.7174 	& 1.004 	& 1.463 	& 1.779 	& 1.779 	& 2.898 	\\
		1st quartile	& 0.7747 	& 1.033 	& 1.549 	& 1.865 	& 1.865 	& 2.955 	\\
		average	& 1.000 	& 1.369 	& 1.744 	& 2.049 	& 2.090 	& 3.144 	\\
		3rd quartile	& 1.090 	& 1.406 	& 1.693 	& 2.008 	& 2.073 	& 3.033 	\\
		maximum	& 1.578 	& 9.096 	& 3.500 	& 3.500 	& 3.414 	& 4.447 	\\
		\bottomrule
	\end{tabular}
	}
\caption{Share analysis}
\end{subfigure}

\vspace{1cm}

\begin{subfigure}[b]{1\textwidth}
	{\addfontfeatures{Numbers={Monospaced,Lining}}
	\begin{tabular}{@{}lD{.}{.}{5}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}D{.}{.}{3}@{}}\toprule
	   program size 	& \multicolumn{1}{c}{1}   	& \multicolumn{1}{c}{2}   	& \multicolumn{1}{c}{3}   	& \multicolumn{1}{c}{4}   	& \multicolumn{1}{c}{5}   	& \multicolumn{1}{c}{7}   	\\
		\midrule
		minimum	& 0.5891 	& 1.588 	& 3.304 	& 6.129 	& 9.820 	& 24.19 	\\
		1st quartile	& 0.6372 	& 1.669 	& 3.507 	& 6.544 	& 10.76 	& 24.66 	\\
		average	& 1.000 	& 2.062 	& 3.802 	& 7.094 	& 11.25 	& 27.30 	\\
		3rd quartile	& 1.046 	& 2.388 	& 3.948 	& 7.075 	& 11.63 	& 26.88 	\\
		maximum	& 3.507 	& 4.347 	& 6.596 	& 11.36 	& 14.24 	& 42.67 	\\
		\bottomrule
	\end{tabular}
	}
\caption{Access analysis}
\end{subfigure}

\caption{Scalability of \textsc{asap}'s analyses for flat programs}
\label{fig:flat-perf}
\end{figure}


The execution time of both the Shape and Access analyses appear exponential in the size of flat programs.
On the other hand, the Share analysis is not – as shown in Figure \ref{fig:graph-flat-share}.
In this Figure, the result for different size are set on the horizontal axis.
For each size of program, a number along with two lines are displayed:
the number value as well as its height represent the average measure;
the line below the number joins the minimum value to the first quartile;
the line above the number joins the third quartile to the maximum value.
Because the results are normalised to the average time, they are presented without a vertical scale.
Note the particularly high value for the maximum time in size two; this outlier may be due to interference during measurements.

\begin{figure}
\includegraphics[width=\textwidth]{plots/flat_share}
\caption{Scalability of the Share analysis for flat programs}
\label{fig:graph-flat-share}
\end{figure}





\subsection{Deep programs}

We programmatically generated programs as presented in Figure \ref{fig:deep-progs}.
We call this category of programs \emph{deep} because the depth of the call grows with the program size.
Specifically, the main function calls a first function which calls a second which calls a third and so on.

\begin{figure}
\begin{alltt}
type tuple = \{ Left: word; Right: word \}

letrec f\(\sb{0}\)(x\(\sb{0}\): word, y\(\sb{0}\): word): tuple =
  {Left = y\(\sb{0}\); Right = x\(\sb{0}\) }
;;

letrec f\(\sb{1}\)(x\(\sb{1}\): word, y\(\sb{1}\): word): tuple =
   f\(\sb{0}\)(y\(\sb{1}\), x\(\sb{1}\))
;;

letrec f\(\sb{2}\)(x\(\sb{2}\): word, y\(\sb{2}\): word): tuple =
   f\(\sb{1}\)(y\(\sb{2}\), x\(\sb{2}\))
;;

(*and so on defining f\(\sb{3}\), f\(\sb{4}\), etc.*)

letrec main(): word =
   let a = f\(\sb{n}\)(4, 7) in
   a.Left + a.Right

\end{alltt}
\caption{Automatically generated deep programs}
\label{fig:deep-progs}
\end{figure}

The performance of each of the three analyses, normalised for deep programs where $n = 1$ is presented in the tables of Figure \ref{fig:deep-perf}.
Specifically, the execution time of all three analyses (Shape, Share, and Access) was measured 75 times for different sizes of program.
For each set of measurements, the average, minimum, maximum, first quartile and third quartile, are normalised to the average for the program of size one.


\begin{figure}
\centering

\begin{subfigure}[b]{1\textwidth}
	{\addfontfeatures{Numbers={Monospaced,Lining}}
	\begin{tabular}{@{}lD{.}{.}{5}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}@{}}\toprule
	   program size 	& \multicolumn{1}{c}{1}   	& \multicolumn{1}{c}{2}   	& \multicolumn{1}{c}{3}   	& \multicolumn{1}{c}{4}   	& \multicolumn{1}{c}{5}   	& \multicolumn{1}{c}{7}   	\\
		\midrule
		minimum	& 0.9414 	& 1.329 	& 1.664 	& 2.022 	& 2.377 	& 3.184 	\\
		1st quartile	& 0.9714 	& 1.345 	& 1.693 	& 2.049 	& 2.441 	& 3.255 	\\
		average	& 1.000 	& 1.387 	& 1.747 	& 2.157 	& 2.523 	& 3.347 	\\
		3rd quartile	& 0.995 	& 1.371 	& 1.766 	& 2.157 	& 2.562 	& 3.341 	\\
		maximum	& 1.567 	& 2.119 	& 2.624 	& 3.249 	& 3.606 	& 5.070 	\\
		\bottomrule
	\end{tabular}
	}
\caption{Shape analysis}
\end{subfigure}

\vspace{1cm}

\begin{subfigure}[b]{1\textwidth}
	{\addfontfeatures{Numbers={Monospaced,Lining}}
	\begin{tabular}{@{}lD{.}{.}{5}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}@{}}\toprule
	   program size 	& \multicolumn{1}{c}{1}   	& \multicolumn{1}{c}{2}   	& \multicolumn{1}{c}{3}   	& \multicolumn{1}{c}{4}   	& \multicolumn{1}{c}{5}   	& \multicolumn{1}{c}{7}   	\\
		\midrule
		minimum	& 0.875 	& 1.039 	& 1.258 	& 1.422 	& 1.586 	& 1.969 	\\
		1st quartile	& 0.930 	& 1.094 	& 1.258 	& 1.477 	& 1.641 	& 2.024 	\\
		average	& 1.000 	& 1.281 	& 1.391 	& 1.612 	& 1.883 	& 2.240 	\\
		3rd quartile	& 0.984 	& 1.258 	& 1.422 	& 1.586 	& 1.750 	& 2.201 	\\
		maximum	& 2.024 	& 2.954 	& 2.133 	& 2.735 	& 4.540 	& 3.446 	\\
		\bottomrule
	\end{tabular}
	}
\caption{Share analysis}
\end{subfigure}

\vspace{1cm}

\begin{subfigure}[b]{1\textwidth}
	{\addfontfeatures{Numbers={Monospaced,Lining}}
	\begin{tabular}{@{}lD{.}{.}{5}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}D{.}{.}{4}@{}}\toprule
	   program size 	& \multicolumn{1}{c}{1}   	& \multicolumn{1}{c}{2}   	& \multicolumn{1}{c}{3}   	& \multicolumn{1}{c}{4}   	& \multicolumn{1}{c}{5}   	& \multicolumn{1}{c}{7}   	\\
		\midrule
		minimum	& 0.9095 	& 1.087 	& 1.266 	& 1.376 	& 1.589 	& 1.913 	\\
		1st quartile	& 0.9168 	& 1.098 	& 1.321 	& 1.415 	& 1.653 	& 1.963 	\\
		average	& 1.000 	& 1.209 	& 1.429 	& 1.531 	& 1.745 	& 2.142 	\\
		3rd quartile	& 0.9437 	& 1.274 	& 1.410 	& 1.520 	& 1.741 	& 2.130 	\\
		maximum	& 1.531 	& 1.848 	& 2.229 	& 2.545 	& 2.697 	& 3.282 	\\
		\bottomrule
	\end{tabular}
	}
\caption{Access analysis}
\end{subfigure}

\caption{Scalability of \textsc{asap}'s analyses for deep programs}
\label{fig:deep-perf}
\end{figure}

The execution time of all three analyses scale better for deep programs than flat programs.
The results are presented in Figures \ref{fig:graph-deep-shape}, \ref{fig:graph-deep-share}, and \ref{fig:graph-deep-access}.

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{plots/deep_shape}
\caption{Scalability of the Shape analysis for deep programs}
\label{fig:graph-deep-shape}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{plots/deep_share}
\caption{Scalability of the Share analysis for deep programs}
\label{fig:graph-deep-share}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{plots/deep_access}
\caption{Scalability of the Access analysis for deep programs}
\label{fig:graph-deep-access}
\end{figure}
