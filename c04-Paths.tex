\chapter{Paths}
\label{sec:paths}
\label{chap:paths}

In order to fill the static-automatic gap (presented in Chapter~\ref{chap:design-space}), we must design a memory management strategy as a compiler pass.
This task requires static analysis of heap properties.
When analysing properties of the heap, it is necessary to have an efficient compile-time abstraction of heap objects.
Indeed, the heap can take an infinite number of configurations: any number of objects can be pointing to each other in any number of ways.
For the purpose of analysis, an abstraction with a bounded-size representation is more useful than an exact representation with unbounded size.

Thus, \emph{paths}: bounded-size\footnote{As detailed in Section~\ref{sec:path-size-bound}, for a value of type $\alpha$, our analyses handles paths of size up to $O(2^s)$ where $s$ is the size of the type definition for $\alpha$.} representations of heap structures.
\index{path}
In our analyses (Chapter~\ref{chap:asap}), paths are used at compile-time to approximate sets of sequences of actual, execution-time accesses.

We first formalise paths and define some operations necessary for our analyses.
We then show how to generate, at compile-time for any given path, code which scans, at execution-time, through the heap along this path.
Because this code is generated with full knowledge of types, it is able to scan values even without runtime types.

\section*{Comparison with similar work}

Paths are similar to the notion of access graphs from the work of Khedker, Sanyal and Karkare \cite{heapref}.
They essentially fill the same role: describing the shape of objects on the heap; but they were designed for different aims and thus fill distinct roles.

Specifically – and as detailed below – paths are designed to be used for multiple analyses.
As a result they are more expressive and are equipped with a more complete set of operations – including a widening operation to ensure termination of analyses that use a fixpoint.
On the other hand, access graphs in \cite{heapref} are designed to compute liveness of heap references – i.e., detect which pointers to a heap object are used by the program later on.
As a result, access graphs are more specialised.

The formalisation of paths (using a regular-expression-like form with a Kleene star ($^{*}$), alternative ($+$) and sequence ($\cdot$)), the operations (partial order, prefix closure, widening, etc.), as well as the code generation (through $\mathit{SCAN}$) are my own.


\section{Formalisation}

A \emph{path} is a regular expression over field and discriminant identifiers.
\index{path}
They are used to describe sets of sequences of memory accesses.
E.g., in Figure~\ref{fig:path-ex-w-list} the path $\mathit{first}$ represents an access to the first element of a list; the path $\mathit{nil}$ represents a recursive descent in a list (denoted by the repetition $^{*}$) to the nil block.
More details follow.

\begin{figure}
	\begin{displaymath}
	\begin{array}{r c l}
		\mathit{unit} & = & \{\}\\
		\mathit{pair} & = & \{\mathit{Left}:\: \mathit{word};\:\mathit{Right}:\: \mathit{word} \}\\
		\mathit{cons} & = & \{\mathit{Head}:\: \mathit{pair};\:\mathit{Tail}:\: \mathit{list} \}\\
		\mathit{list} & = & \mathit{Cons}\: \mathit{cons} + \mathit{Nil}\: \mathit{unit}\\
		\\
		\mathit{first} & = & \mathit{Cons} \cdot \mathit{Head}\\
		\mathit{elems} & = & (\mathit{Cons} \cdot \mathit{Tail})^{*} \cdot \mathit{Cons} \cdot \mathit{Head}\\
		\mathit{spine} & = & (\mathit{Cons} \cdot \mathit{Tail})^{*} \cdot \mathit{Cons}\\
		\mathit{nil} & = & (\mathit{Cons} \cdot \mathit{Tail})^{*} \cdot \mathit{Nil}
	\end{array}
	\end{displaymath}
	\caption{Example of paths for the $\mathit{list}$ type}
	\label{fig:path-ex-w-list}
\end{figure}


\subsection{Grammar}

The grammar of paths and zones is defined in Figure~\ref{fig:path-grammar}.
A field identifier ($F \in \mathit{fieldname}$) indicates a dereference to the corresponding field.
A discriminant identifier ($D \in \mathit{discriminantname}$) corresponds to a dynamic check to determine what element of a sum is being considered – more below.

Note that the formal definition of paths, as well as each of the auxiliary definitions below, depends on the type system for the considered language.
Specifically, paths include identifiers from $\mathit{fieldname}$ and $\mathit{discriminantname}$.
We only present paths for the values of µL because they are the ones we use in \textsc{asap}.
However, we posit the path abstraction can be adapted to other languages with other type systems.

\begin{figure}
\begin{displaymath}
\begin{array}{r l}
\mathit{path}::= & \: \epsilon \\
	| &\: \mathit{fieldname} \\
	| &\: \mathit{discriminantname} \\
	| &\: \mathit{path} \cdot \mathit{path} \quad \hfill \text{(sequence)} \\
	| &\: \mathit{path} + \mathit{path} \quad \hfill \text{(alternative)} \\
	| &\: \mathit{path}^{*} \quad \hfill \text{(repetition)} \\
\mathit{zone}::= & (\mathit{variable}, \mathit{path})
\end{array}
\end{displaymath}
\caption{The grammar of paths}
\label{fig:path-grammar}
\end{figure}

\subsection{Type compatibility}
\label{sec:paths-and-types}

The operator $\alpha . p$, where $\alpha$ is a type name and $p$ is a path, is defined in Figure~\ref{fig:path-apply-apply} where the global map $\Delta : \mathit{typename} \rightarrow \tau$ associates type names with their definitions.
The result of $\alpha . p$ is the type of the values represented by memory blocks reached from values of type $\alpha$ following sequences of dereference recognised by $p$.
E.g., $\mathit{list} . \mathit{Cons}$ is the type $\mathit{cons}$, $\mathit{list} . \mathit{Cons} \cdot \mathit{Tail}$ is the type $\mathit{list}$, and $\mathit{list} . (\mathit{Cons} \cdot \mathit{Tail})^{*}$ is also the type $\mathit{list}$.

Note that the operator is partial: not all paths apply to all types.
Specifically, $\alpha . p$ is defined only when $p$ recognises sequences of dereferences that can be applied on values of type $\alpha$.
E.g., a path describing a descent into a tree does not apply to the type of lists.


We say that two paths $p_1$ and $p_2$ are type-compatible when they apply to all types similarly.
We write $\mathit{compatible}(p_1,p_2)$ for compatibility between $p_1$ and $p_2$ and $\mathit{compatible}(P)$ for pairwise compatibility of paths in the set $P$.
Type-compatibility is defined in Figure~\ref{fig:path-apply-compat}.
\index{path!compatible}

\begin{figure}
\begin{subfigure}{1\textwidth}
	\begin{displaymath}
	\begin{array}{r c l}
		\_ . \_ & : & \mathit{typename} \times \mathit{path} \rightarrow \mathit{typename}\\
		\alpha . \epsilon & = & \alpha\\
		\alpha . F & = & \beta \qquad \text{if}\: \Delta(\alpha) = \{ \dots ; F : \beta ; \dots \} \\
		\alpha . D & = & \beta \qquad \text{if}\: \Delta(\alpha) = \dots + D\: \beta + \dots \\
		\alpha . (p_1 \cdot p_2) & = & (\alpha . p_1 ) . p_2\\
		\alpha . (p_1 + p_2) & = & \beta \qquad \text{if}\: \alpha . p_1 = \alpha . p_2 = \beta \\
		\alpha . p^{*} & = & \alpha \qquad \text{if}\: \alpha . p = \alpha \\
		\alpha . p & & \text{undefined otherwise}
	\end{array}
	\end{displaymath}
	\caption{Applying paths to types}
	\label{fig:path-apply-apply}
\end{subfigure}
\\
\begin{subfigure}{1\textwidth}
	\begin{displaymath}
	\begin{array}{r c l}
		\mathit{compatible}(p_1, p_2) & \iff & \forall \alpha, \alpha . p_1 = \alpha . p_2 \vee (\alpha . p_1 \text{ and } \alpha . p_2 \text{ undefined}) \\
		\mathit{compatible}(P) & \iff & \forall p, q \in P, \mathit{compatible}(p, q)
	\end{array}
	\end{displaymath}
	\caption{Type compatibility between pairs or sets of paths}
	\label{fig:path-apply-compat}
\end{subfigure}
\caption{Path application and type compatibility}
\label{fig:path-apply}
\end{figure}

\subsection{Zones during execution}

Given a location $l$ of a value of type $\alpha$, a path $p$, a heap $\eta$ and a stack $\sigma$, we write $Z(l,\alpha,p)(\eta,\sigma)$ for the set of memory blocks reachable from $l$, following sequences of dereferences recognised by the path $p$.
It is formally defined in Figure~\ref{fig:path-formal}.
In the definition, we write $\pi_F$ for the projection for the field $F$ – remember that blocks are tuples of $\mathit{address} \cup \mathit{word}$ – and we also write $\pi_D$ for the projection for the variant $D$: for a memory block at location $l$ that represents the value $D\: x$, the location of $x$ is $\pi_D(l)$.

\begin{figure}
\begin{displaymath}
	\begin{array}{r c l}
		\multicolumn{3}{c}{Z : \mathit{location} \times \mathit{typename} \times \mathit{path} \rightarrow \mathit{heap} \times \mathit{stack} \rightarrow 2^{\mathit{location}}}\\
		Z(l,\alpha,\epsilon)(\eta,\sigma) & = & \{l\} \\
		Z(l,\alpha,F)(\eta,\sigma) & = &
			\begin{cases*}
				\emptyset  & if $\alpha . F = \mathit{word}$ \\
				\{\pi_{F}(l)\} & otherwise
			\end{cases*}\\
		Z(l,\alpha,D)(\eta,\sigma) & = &
			\begin{cases*}
				\emptyset & if $\alpha . D = \mathit{word}$ \\
				\emptyset & if the variant at $l$ is not $D$\\
				\{\pi_D(l)\} & otherwise
			\end{cases*}\\
		Z(l,\alpha,p \cdot p')(\eta,\sigma) & = &
			\left\{
				Z(l',\alpha',p')(\eta,\sigma)
			\:\middle|
				\begin{array}{l}
					\alpha' = \alpha . p,\\
					l' \in Z(l,\alpha,p)(\eta,\sigma)
				\end{array}
			\right\} \\
		Z(l,\alpha,p + p')(\eta,\sigma) & = & Z(l,\alpha,p)(\eta,\sigma) \cup Z(l,\alpha,p')(\eta,\sigma) \\
		Z(l,\alpha,p^{*})(\eta,\sigma) & = &
			\begin{array}[t]{l}
				\bigcup_{n \in \mathbb{N}} Z_n \\
				\text{ where }
				\left\{\begin{array}{l}
					Z_0 = Z(l,\alpha,\epsilon)(\eta,\sigma) \\
					Z_{n+1} = \{ Z(l',\alpha,p)(\eta,\sigma) \mid l' \in Z_n \}
				\end{array}\right .
			\end{array}
	\end{array}
\end{displaymath}
\caption{The set of locations $Z(l,\alpha,p)(\eta,\sigma)$}
\label{fig:path-formal}
\end{figure}

Notice how some arguments of $Z$ are available at compile-time but others are only available at execution-time.
We implement a staged function that corresponds to $Z$ in Section~\ref{sec:SCAN}.

We overload $Z$ and often simply write $Z(x,p)(\eta,\sigma)$ for the more complete form $Z(\sigma(x),\Gamma(x),p)(\eta,\sigma)$ – remember that $\Gamma$ maps variables to their type.
This overloading is more than purely æsthetic: in the shorter form we use the compile-time identifier for the scanned value (i.e., the variable $x$) rather than the execution-time location ($l$).
Thus, we can talk at compile-time about the set of memory blocks in a zone $(x,p)$ and defer the location resolution ($\sigma(x)$) to the execution.
Specifically, it allows us to use $Z(z)$ (where $z$ is a zone) to describe, during analyses, properties of the heap.

\section{Examples}

Consider a variable $x$ of type $\mathit{list}$ and the paths $\mathit{first}$, $\mathit{elems}$ and $\mathit{spine}$ defined in Figure~\ref{fig:path-ex-w-list}.

The set $Z(x,\mathit{first})(\eta, \sigma)$ is empty if the list $x$ is empty, or it contains the first element of the list $x$ otherwise.
Also note that $\mathit{list} . \mathit{first} = \mathit{pair}$, which coincides with the fact that the first element of the list is of type $\mathit{pair}$.

The set $Z(x,\mathit{elems})(\eta,\sigma)$ contains all the elements of the list $x$.
Once again, the type $\mathit{list} . \mathit{elems} = \mathit{pair}$ coincides with the type of the elements of the list.

Finally, the set $Z(x,\mathit{spine})(\eta,\sigma)$ contains all the cons cells of the list $x$.


\section{Use for analysis}

In order to carry out the heap analyses of \textsc{asap} in Chapter~\ref{chap:asap}, we need to provide a few operations on paths.
Specifically, our analyses rely on fixpoints in the \textsc{3vl}-sets of zones ($3^{(\mathit{variable} \times \mathit{path})}$).

A common pattern during the fixpoint operations is the accumulation of values such as $\{(x,\epsilon), (x,p), (x,p \cdot p), (x, p \cdot p \cdot p), \dots \}$.
We define a widening operator $\mathit{Widen}$ that collapses these accumulations into $\{(x,p^{*})\}$.
These accumulations appear when analysing a recursive function that descends into a value (of a recursive type) along the path $p$.
E.g., a function that computes the length of a list will cause the accumulation $\{(x,\epsilon), (x,\mathit{Cons} \cdot \mathit{Tail}), (x,\mathit{Cons} \cdot \mathit{Tail} \cdot \mathit{Cons} \cdot \mathit{Tail}), \dots \}$ and is collapsed into $\{ (x,(\mathit{Cons} \cdot \mathit{Tail}) ^{*}) \}$.

Before we can define the widening operator, we need several helper functions which we define now.



\subsection{Partial order}

We define the partial order $\preceq$ on paths in Figure~\ref{fig:paths-compare}.
By design, the definition is such that $p \preceq p' \Rightarrow \forall x, \eta, \sigma, Z(x,p)(\eta,\sigma) \subseteq Z(x,p')(\eta,\sigma)$.

\begin{figure}
\begin{displaymath}
\begin{array}{c c l}
p \preceq p' & \Leftarrow & p = p'\\
\epsilon \preceq p^{*} & \Leftarrow & \mathit{compatible}(p, p \cdot p) \\
p_1 + \dots + p_n \preceq p'_1 + \dots + p'_{m} & \Leftarrow & \forall i \leq n, \exists j \leq m, p_i \preceq p'_j \\
p^{*} \preceq p'^{*} & \Leftarrow & p \preceq p'\\
p \cdot q \preceq p^{*} & \Leftarrow & q \preceq p^{*} \\
p_1 \cdot p_2 \preceq p'_1 \cdot p'_2 & \Leftarrow & p_1 \preceq p'_1 \:\text{and}\: p_2 \preceq p'_2
\end{array}
\end{displaymath}
\caption{Axioms of path comparison}
\label{fig:paths-compare}
\end{figure}

Note that Figure~\ref{fig:paths-compare} hints at the implementation of the comparison: it reads as pseudo-Prolog where the left-hand side is destructed and the (smaller) right-hand side is tested recursively.

Also note that in the sequence case ($p_1 \cdot p_2 \preceq p'_1 \cdot p'_2$) it is only ever useful to consider cases where $p_1$ and $p'_1$ are type compatible because the order $\preceq$ is only defined on type compatible pairs of paths.
This consideration is useful when comparing long sequences, to decide where to split the paths.


\subsection{Prefix closure}

We define the function $\mathit{Prefix}$ as follows: $\forall p \in \mathit{path}$, $\mathit{Prefix}(p)$ is a set of paths such that $\bigcup_{q \in \mathit{Prefix}(p)} L(q)$ is the language which contains all the prefixes of words of $L(p)$.
The function is defined in Figure~\ref{fig:path-prefix}.

\begin{figure}
\begin{displaymath}
\begin{array}{r c l}
\mathit{Prefix} : \mathit{path} & \rightarrow & 2^{\mathit{path}} \\
\mathit{Prefix}(\epsilon) & = & \{ \epsilon \} \\
\mathit{Prefix}(F) & = & \{ F, \epsilon \} \\
\mathit{Prefix}(D) & = & \{ D, \epsilon \} \\
\mathit{Prefix}(p \cdot q) & = & \mathit{Prefix}(p) \cup \{ p \cdot q' \mid q' \in \mathit{Prefix}(q) \} \\
\mathit{Prefix}(p + q) & = & \mathit{Prefix}(p) \cup \mathit{Prefix}(q) \\
\mathit{Prefix}(p^{*}) & = & \mathit{Prefix}(p) \cup \{ p^{*} \cdot p' \mid p' \in \mathit{Prefix}(p) \}
\end{array}
\end{displaymath}
\caption{$\mathit{Prefix}$ of path}
\label{fig:path-prefix}
\end{figure}

We often use $\mathit{Prefix}(p)$ (which is technically a set of paths) where a path is expected – e.g., we write $p \cdot \mathit{Prefix}(q)$ or $[ (x,\mathit{Prefix}(p)) \mapsto \top]$.
The intuition behind this abuse of notation is that the set of paths is used instead of the alternation of its elements – i.e., $p \cdot \mathit{Prefix}(q)$ stands for $p \cdot \Sigma \mathit{Prefix}(q)$ where $\Sigma \{p_1, \dots, p_n\} = p_1 + \ldots + p_n$.

Note, however, that the elements of $\mathit{Prefix}(p)$ are not type-compatible in general – e.g., the prefix for the path selecting the elements of a list will select blocks that represent the elements of the list, but also blocks that represent the cons cells of the list.
Given incompatible types $\{p_1, \dots, p_n\}$, the application $\alpha . p_1 + \ldots + p_n$ is undefined for any type name $\alpha$.
Thus we give an arguably less intuitive but better-behaved interpretation of the set-of-paths-as-path notation:
we write $p \cdot \mathit{Prefix}(q)$ for the notationally correct $\{ p \cdot q' \mid q' \in \mathit{Prefix}(q) \}$ and we write $[ (x,\mathit{Prefix}(p)) \mapsto \top]$ for $[ (x,p') \mapsto \top \mid p' \in \mathit{Prefix}(p) ]$.
In other words, we use a set of paths as a path where a guard in an intensional definition would be more correct.


\subsection{Wild path set}
\label{sec:wild-path}

We write $\mathit{Wild}(\alpha)$ for the \emph{wild} path set of the type $\alpha$.
\index{wild path set}
The wild path set of $\alpha$ is a set of paths that, collectively, explores the whole memory occupied by a value of type $\alpha$.
More formally, we require that $\forall \eta, \sigma, \bigcup_{p \in \mathit{Wild}(\Gamma(x))}Z(x,p)(\eta, \sigma)$ comprises all the memory locations that are reachable from $x$.
(Note, that this property does not define a unique wild path set for each type.
However, we show below how to compute a particular solution, which we call \emph{the} wild path set.)

Note that, just as with prefix closure, the different paths of a wild path set are not, in general, type-compatible.
Indeed, the memory blocks that constitute a value, say a list, represent values of different types, say cons cells and elements.
As a result, we use the same notation as with prefix sets: we write $[(x,\mathit{Wild}(\Gamma(x))) \mapsto \top]$ for $[(x,p) \mapsto \top \mid p \in \mathit{Wild}(\Gamma(x))]$.

\paragraph{Example: trees of lists of pairs}

We first show the wild path set for a simple but non-trivial example: a type including several tiers and recursions.
The type $\mathit{tree}$, along with the other intermediate types it relies on, is defined in Figure~\ref{fig:wild-ex-tlp-def}.
The type is given the visual representation of a graph in Figure~\ref{fig:wild-ex-tlp-graph} to help form intuition.
Finally, the wild path set for the type is given in Figure~\ref{fig:wild-ex-tlp-res}.

\begin{figure}
\begin{subfigure}{1\textwidth}
	\begin{displaymath}
	\begin{array}{l}
		\mathit{unit} = \{\}\\
		\mathit{pair} = \{\mathit{Left}:\: \mathit{word};\:\mathit{Right}:\: \mathit{word} \}\\
		\\
		\mathit{cons} = \{\mathit{Head}:\: \mathit{pair};\:\mathit{Tail}:\: \mathit{list} \}\\
		\mathit{list} = \mathit{Cons}\: \mathit{cons} + \mathit{Nil}\: \mathit{unit}\\
		\\
		\mathit{node} = \{\mathit{LBr}:\: \mathit{tree};\:\mathit{Val}:\: \mathit{list}; \:\mathit{RBr}:\: \mathit{tree} \}\\
		\mathit{tree} = \mathit{Node}\: \mathit{node} + \mathit{Leaf}\: \mathit{unit}
	\end{array}
	\end{displaymath}
	\caption{Definition of the $\mathit{tree}$ type}
	\label{fig:wild-ex-tlp-def}
\end{subfigure}
\\
\begin{subfigure}{1\textwidth}
	\centering
	\begin{tikzpicture}[align=center, node distance=2cm, auto]
		\node(word){$\mathit{word}$};
		\node(pair)[left of=word]{$\mathit{pair}$};
		\draw[->] (pair) edge[bend left=45] node [auto] {$\mathit{Left}$} (word);
		\draw[->] (pair) edge[bend right=45] node [below] {$\mathit{Right}$} (word);

		\node(cons)[left of=pair]{$\mathit{cons}$};
		\node(list)[left of=cons]{$\mathit{list}$};
		\draw[->] (list) edge node [auto] {$\mathit{Cons}$} (cons);
		\draw[->] (cons) edge node [auto] {$\mathit{Head}$} (pair);
		\draw[->] (cons) edge[bend right=90] node [above] {$\mathit{Tail}$} (list);

		\node(nd)[left of=list]{$\mathit{node}$};
		\draw[->] (nd) edge node [auto] {$\mathit{Val}$} (list);
		\node(tree)[left of=nd]{$\mathit{tree}$};
		\draw[->] (nd) edge[bend right=90] node [above] {$\mathit{LBr}$,$\mathit{RBr}$} (tree);
		\draw[->] (tree) edge node [auto] {$\mathit{Node}$} (nd);

		\node(unit)[below of=nd, node distance=1cm]{$\mathit{unit}$};
		\draw[->] (list) edge node [auto] {$\mathit{Nil}$} (unit);
		\draw[->] (tree) edge node [left] {$\mathit{Leaf}$} (unit);
	\end{tikzpicture}
	\caption{Graph representation of the $\mathit{tree}$ type}
	\label{fig:wild-ex-tlp-graph}
\end{subfigure}
\\
\begin{subfigure}{1\textwidth}
	\begin{displaymath}
	\begin{array}{r c l}
		\mathit{Wild}(\mathit{pair}) & = & \{ \epsilon, \mathit{Left}, \mathit{Right}, \mathit{Left} + \mathit{Right} \} \\
		\\
		\mathit{Wild}(\mathit{list}) & = & \mathit{suffixes} \cup \mathit{descending} \\
		\text{where } \mathit{suffixes} & = & \{ \epsilon, \mathit{Nil}, \mathit{Cons}, \mathit{Cons} \cdot \mathit{Tail} \}\\
		& & \qquad\cup \{ \mathit{Cons} \cdot \mathit{Head} \cdot p \mid p \in \mathit{Wild}(\mathit{pair}) \} \\
		\text{and } \mathit{descending} & = & \{ (\mathit{Cons} \cdot \mathit{Tail})^{*} \cdot s \mid s \in \mathit{suffixes} \} \\
		\\
		\mathit{Wild}(\mathit{tree}) & = & \mathit{suffixes} \cup \mathit{descending} \\
		\text{where } \mathit{suffixes} & = &
		\{ \epsilon, \mathit{Leaf}, \mathit{Node}, \mathit{Node} \cdot \mathit{RBr}, \mathit{Node} \cdot \mathit{LBr},\\
		& & \qquad \mathit{Node} \cdot (\mathit{RBr} + \mathit{LBr}) \} \\
		& & \qquad \cup \{ \mathit{Node} \cdot \mathit{Val} \cdot l \mid l \in \mathit{Wild}(\mathit{list}) \} \\
		\text{and } \mathit{descending} & = &
		\left\{ \begin{array}{l}
			(\mathit{Node} \cdot \mathit{LBr})^{*} \cdot s, \\
			(\mathit{Node} \cdot \mathit{RBr})^{*} \cdot s, \\
			(\mathit{Node} \cdot \mathit{LBr} + \mathit{Node} \cdot \mathit{RBr})^{*} \cdot s
		\end{array} \middle| \:
			s \in \mathit{suffixes}
		\right\} \\
	\end{array}
	\end{displaymath}
	\caption{Wild path set for the $\mathit{tree}$ type}
	\label{fig:wild-ex-tlp-res}
\end{subfigure}
\\
\caption{Example: type $\mathit{tree}$ with its graph representation and wild path set}
\label{fig:wild-ex-tlp}
\end{figure}


\paragraph{Computation}

To compute the wild path set of a type $\tau$ named $\alpha$, we start by computing two sets of paths, $R$ and $S$, based on the type definition.

The paths of $R$ represent type recursion: they lead from a value of type $\alpha$ back to a value of type $\alpha$ without visiting intermediate values of type $\alpha$.
Formally,
$$\forall r \in R, \alpha . r = \alpha \wedge \forall r' \in \mathit{Prefix}(r) \setminus \{r\}, \alpha . r' \neq \alpha$$
They are computed by searching for (recursive) occurrences of $\alpha$ in $\tau$.

The paths of $S$ are the non-recursive suffixes; they are computed as follows where $\alpha_i \setminus \alpha$ is $\alpha_i$ where any reference to $\alpha$ is removed.
More specifically, the type $\alpha_i \setminus \alpha$ is obtained by pruning the definition of $\alpha_i$ of all the leaves $\alpha$.
$$
S =
\begin{dcases}
	\{ F_i \cdot w \mid i \leq n, w \in \mathit{Wild}(\alpha_i \setminus \alpha) \} & \text{if } \tau = \{ F_1: \alpha_1;\: \dots;\: F_n: \alpha_n \} \\
	\{ D_i \cdot w \mid i \leq n, w \in \mathit{Wild}(\alpha_i \setminus \alpha) \} & \text{if } \tau = D_i\: \alpha_i + \dots + D_n\: \alpha_n \\
	\{ \} & \text{if } \tau = \mathit{word}
\end{dcases}
$$

Using these two sets $R$ and $S$ we define the wild path set as follows where $\Sigma \{p_1, \dots, p_n\} = p_1 + \ldots + p_n$.
$$\mathit{Prefix}(\{(\Sigma R')^{*} \cdot (\Sigma S') \mid R' \subseteq R, S' \subseteq S \})$$



\subsection{Widening}

The widening operator transforms a set of type-compatible paths into a single path.
It is used during \textsc{asap} analyses; specifically, it is used during the fixpoint computations to ensure termination.


The $\mathit{Widen}$ operation takes a set of type-compatible paths and produces a single path.
It is such that $\forall i, p_i \preceq \mathit{Widen}(\{p_1, \dots, p_n\})$ – which implies that $\forall v, i, \eta, \sigma$, we have $Z(v,p_i)(\eta,\sigma) \subseteq Z(v,\mathit{widen}(\{p_1, \dots, p_n\}))(\eta,\sigma)$.
That is, the widening of a set of paths is a singular path that subsumes them all.
Note however, that widening is not defined for arbitrary sets of paths: they must be type compatible.

Given a set of zones $\{(x,p_1), \dots, (x,p_n)\}$, the path $\mathit{Widen}(\{p_1, \dots, p_n\})$ is the smallest element $p_0$ of the wild path set $\mathit{Wild}(\Gamma(x))$ such that $\forall i, p_i \preceq p_0$.
Note that widening is always used for sets of zones that pertain to a single variable (here $x$) which makes it possible to compute the appropriate wild path set (here $\mathit{Wild}(\Gamma(x))$).

Note that our widening operator erases more information than is strictly necessary for termination.
E.g., consider the set of zones $e = \{(x, \epsilon), (x, p \cdot p), (x, p \cdot p \cdot p \cdot p), \dots \}$ where the sub-path $p$ always appears an even number of times; widening loses the parity information: $\mathit{Widen}(e) = \{ (x, p^{*}) \}$.


\subsection{Size bound}
\label{sec:path-size-bound}

Our analysis always uses paths of bounded size; we compute the bound now.
There are two cases to consider.
First, in non-recursive functions, \textsc{asap}'s analyses happens in one pass over the body of the function.
During the analysis, both constructors and destructors introduce paths that are bigger than previously known paths.
However, the size increment on the paths is bounded by a constant (see later the specific $\mathit{Transfer}$ functions).
Thus, in a non-recursive function, both the number and the length of paths in the analysis result grow linearly with the size of the function.

Second, in recursive functions, \textsc{asap}'s analyses use the widening operator between each iteration of the fixpoint computation.
Note that the result of a widening is a path that is a member of the wild path set for the associated type.
Paths in the wild set are bounded in size.
Specifically, they are at most as long as the concatenation of their longest recursive branch and their longest non-recursive branch and they are at most as wide as the number of type-compatible segments.



\section{Use for $\mathit{SCAN}$ning}
\label{sec:SCAN}

Paths contain enough information to generate code that scans through a given part of the heap.
We write $\mathit{SCAN}(x,\alpha,p,\mathit{PRE},\mathit{FIN})$ for the scanning code that starts at the variable $x$ of type $\alpha$ and explores every memory block reachable through dereferences described by the path $p$.
During the exploration, this scanning code uses $\mathit{FIN}$ on all the memory blocks of $Z(x,p)(\eta,\sigma)$.
The scanning code also uses $\mathit{PRE}$ on every memory block it visits – i.e., on all the blocks of $Z(x,\mathit{Prefix}(p))(\eta,\sigma)$.
The mnemonics for these compile-time functions is that $\mathit{FIN}$ stands for “final” and $\mathit{PRE}$ for “prefix”.
Note that $\mathit{PRE}$ is also used on the blocks of $Z(x,p)(\eta,\sigma)$ because $p \in \mathit{Prefix}(p)$.

We use capitalised identifiers for $\mathit{SCAN}$, $\mathit{PRE}$ and $\mathit{FIN}$ because they are compiler functions, not program functions.
They are like macros; they do not appear at run time but, instead, are transformed by the compiler into low-level code.
The function $\mathit{SCAN}$ recurses at compile time over bounded-size values: types and paths.
The result of that function is code (in µL) that, at execution time, recurses on potentially unbounded-size (but always finite) values.
In other words, the function $\mathit{SCAN}$ is the algorithmic counterpart to the mathematical definition of $Z$ in Figure~\ref{fig:path-formal}.
It is a \emph{staged function} implementation of $Z(l,\alpha,p)(\eta,\sigma)$ where $l$, $\alpha$ and $p$ are compile-time arguments and $\eta$ and $\sigma$ are execution-time arguments.
Details about staging can be sought in Taha's introduction to multi-stage programming \cite{metaocaml}.

Note that values of the $\mathit{word}$ type are treated differently – which is also the case in the definition of $Z$.
These values are stored inline inside memory blocks or directly on the stack.
Memory management is about the allocation and deallocation of blocks in the heap, not their content.
Thus, the tests for types in the definition of $\mathit{SCAN}$ below simply skips inline word values.
Remember that these tests are not performed during execution; instead they inform a compile-time decision about code generation.
Notice, specifically, how the tests appear in the definition but not the example of $\mathit{SCAN}$.

\subsection{Example}

Figure~\ref{fig:scan-list-elems} shows the synthesised code for the $\mathit{elems} = (\mathit{Cons} \cdot \mathit{Tail})^{*} \cdot \mathit{Cons} \cdot \mathit{Head}$ path of a value of type $\mathit{list}$.
This scanning code explores a whole list; it calls $\mathit{FIN}$ on the elements of the list and $\mathit{PRE}$ on every block it traverses.
The code includes hand-written comments and syntactic sugar for the purpose of readability.
The delimiters $\langle$ and $\rangle$ denote (staging) code quotations whilst the construct $\ESC{e}$ denotes anti-quotations – as per MetaOCaml syntax \cite{metaocaml}.

The $\mathit{SCAN}$ning code consists of two important functions.
The first one is $\mathit{descend}$ which corresponds to the repeated portion of the path ($(\mathit{Cons} \cdot \mathit{Tail})^{*}$).
On each cons cell of the list it calls the second important function: $\mathit{work}$.
This second one resolves the $\mathit{Head}$ component of the cons cell to get the corresponding element of the list.
On each element of the list, it calls the function $\mathit{FIN}$.


\begin{figure}
\begin{displaymath}
\begin{array}{l}
	\mathit{SCAN}(x, \mathit{list},((\mathit{Cons} \cdot \mathit{Tail})^{*} \cdot \mathit{Cons} \cdot \mathit{Head}),\mathit{PRE},\mathit{FIN}) =\\
\qquad \langle \mathit{descend}\texttt{(}x\texttt{)} \rangle\\
\\
\text{where the function $\mathit{descend}$ is generated as} \\
\texttt{fun}\: \mathit{descend}\texttt{(}x\texttt{)} \texttt{=} \\
\qquad  \texttt{(*this function implements } (\mathit{Cons} \cdot \mathit{Tail})^{*} \texttt{ *)}\\
\qquad  \ESC{\mathit{PRE}(x,\mathit{list})}\texttt{;}\\
\qquad  \texttt{match}\:  x\: \texttt{with}\\
\qquad  \qquad \texttt{[}\: \mathit{Cons}\: x'\: \texttt{->}\\
\qquad  \qquad \qquad \ESC{\mathit{PRE}(x',\mathit{list}.\mathit{Cons})}\texttt{;}\\
\qquad  \qquad \qquad \texttt{match}\:  x'\: \texttt{with}\: \{\mathit{Tail}\texttt{=}x''\}\: \texttt{->}\\
\qquad  \qquad \qquad \qquad \ESC{\mathit{PRE}(x'',\mathit{list}.\mathit{Cons}\cdot\mathit{Tail})}\texttt{;}\\
\qquad  \qquad \qquad \qquad \texttt{(*this recursion implements the repetition*)}\\
\qquad  \qquad \qquad \qquad \mathit{descend}\texttt{(}x''\texttt{)} \\
\qquad  \qquad \texttt{|}\: \texttt{\_}\: \texttt{->}\: \texttt{\{\}}\:\texttt{(* Nil: don't do anything *)}\\
\qquad  \qquad \texttt{];}\\
\qquad  \texttt{(*continue with the element*)}\\
\qquad  \mathit{work}\texttt{(}x\texttt{)}\\
\\
\text{and the function $\mathit{work}$ is generated as} \\
\texttt{fun}\: \mathit{work}\texttt{(}x\texttt{)} \texttt{=} \\
\qquad  \texttt{(*this function implements }\mathit{Cons} \cdot \mathit{Head}\texttt{ *)}\\
\qquad  \texttt{match}\:  x\: \texttt{with}\\
\qquad  \qquad \texttt{[}\: \mathit{Cons}\: x'\: \texttt{->}\\
\qquad  \qquad \qquad \ESC{\mathit{PRE}(x',\mathit{list}.\mathit{Cons}\cdot\mathit{Head})}\texttt{;}\\
\qquad  \qquad \qquad \texttt{match}\:  x'\: \texttt{with}\: \{\mathit{Head}\texttt{=}x''\}\: \texttt{->}\\
\qquad  \qquad \qquad \qquad \ESC{\mathit{PRE}(x'',\mathit{list}.\mathit{Cons}\cdot\mathit{Tail})}\texttt{;}\\
\qquad  \qquad \qquad \qquad \texttt{(* accepting state: use }\mathit{FIN}\texttt{(al) action *)}\\
\qquad  \qquad \qquad \qquad \ESC{\mathit{FIN}(x'',\mathit{list}.\mathit{Cons}\cdot\mathit{Tail})} \\
\qquad  \qquad \texttt{|}\: \texttt{\_}\: \texttt{->}\: \texttt{\{\}} \:\texttt{(* Nil: don't do anything *)}\\
\qquad  \qquad \texttt{]}
\end{array}
\end{displaymath}
\caption{Example of synthesised scanning code}
\label{fig:scan-list-elems}
\end{figure}

\subsection{Formal definition}

The $\mathit{SCAN}$ function is formally defined in Figure~\ref{fig:scan-def}.
The function takes a variable ($x$), a type name ($\alpha$), a path ($p$) and two compile-time functions ($\mathit{PRE}$ and $\mathit{FIN}$).
Its domain-codomain signature is:
\[
\mathit{SCAN} :
\left(\begin{array}{l}
		\mathit{variable}
		\times \mathit{typename} \\
		\times \mathit{path} \\
		\times (\mathit{variable} \times \mathit{typename} \rightarrow \text{µL}) \\
		\times (\mathit{variable} \times \mathit{typename} \rightarrow \text{µL})
\end{array}\right)
		\rightarrow \text{µL}
\]

At compile-time $\mathit{SCAN}$ mechanically produces code that, when executed, scans the value held by $x$ of type $\alpha$ along the path $p$.
Note that we require $\mathit{PRE}$ to be idempotent – typically “set a mark bit to \texttt{1}” or “record that address in this set.”
The definition uses MetaOCaml \cite{metaocaml} syntax to distinguish compile-time and execution-time elements.
Specifically, the notation $\langle$ and $\rangle$ is used to quote code and $\ESC{e}$ to escape $e$.



The $\mathit{SCAN}$ function matches over the path and generates appropriate code.
For the trivial path $\epsilon$: use the $\mathit{PRE}$ and $\mathit{FIN}$ functions except for values of type $\mathit{word}$.
For the simple field path $F$: use $\mathit{PRE}$ on the current block, then, if the path leads to a block rather than a $\mathit{word}$, dereference and continue.
For the simple discriminant path $D$: use $\mathit{PRE}$ on the current block, then, if the path leads to a block rather than a $\mathit{word}$ and if the block matches $D$, dereference and continue.
For the sequence path $p \cdot p'$: explore $p$ but replace $\mathit{FIN}$ by a continuation ($\mathit{CONT}$) that explores $p'$.
For the alternative path $p + p'$: explore $p$, then explore $p'$.
For the repetition path $p^{*}$: use a loop\footnote{Remember than in µL loops are expressed as in $\textsc{cps}$: with a recursive function.} to descend through $p$ and call $\mathit{FIN}$ on each of the blocks.

Note the similarity with the formal definition of $Z$ in Figure~\ref{fig:path-formal}.
The main difference is the inclusion of $\mathit{PRE}$.

\begin{figure}
\begin{displaymath}
\begin{array}[t]{l}
\mathit{SCAN}\left(\!\!\!
	\begin{array}{c}
		x,\alpha,\epsilon,\\
		\mathit{PRE},\mathit{FIN}
	\end{array}\!\!\!\right) =
	\begin{array}[t]{l}
		\texttt{if}\: \Delta(\alpha) \neq \mathit{word}\: \texttt{then}\\
		\qquad \langle\ESC{\mathit{PRE}(x,\alpha)}\texttt{;}\: \ESC{\mathit{FIN}(x,\alpha)}\rangle \\
		\texttt{else}\\
		\qquad \langle\texttt{\{\}}\rangle
	\end{array}
\\

\mathit{SCAN}\left(\!\!\!
	\begin{array}{c}
		x,\alpha,F,\\
		\mathit{PRE},\mathit{FIN}
	\end{array}\!\!\!\right) =
	\begin{array}[t]{l}
	\texttt{if}\: \alpha . F \neq \mathit{word}\: \texttt{then}\\
		\qquad \stretchleftright[1000]{\langle}{\begin{array}{l}
			\ESC{\mathit{PRE}(x,\alpha)}\texttt{;}\\
			\texttt{match}\:  x\: \texttt{with}\\
				\qquad\texttt{[} \: \texttt{\{}F\texttt{=}y\texttt{\}}\: \texttt{->}\\
				\qquad \qquad\ESC{\mathit{PRE}(y,\alpha . F)}\texttt{;}\\
				\qquad \qquad\ESC{\mathit{FIN}(y,\alpha . F)}\\
				\qquad\texttt{]}
		\end{array}}{\rangle} \\
	\texttt{else}\\
		\qquad \langle\ESC{\mathit{PRE}(x,\alpha)}\texttt{;}\: \ESC{\mathit{FIN}(x,\alpha)}\rangle
	\end{array}
\\

\mathit{SCAN}\left(\!\!\!
	\begin{array}{c}
		x,\alpha,D,\\
		\mathit{PRE},\mathit{FIN}
	\end{array}\!\!\!\right) =
	\begin{array}[t]{l}
	\texttt{if}\: \alpha . D \neq \mathit{word}\: \texttt{then}\\
		\qquad \stretchleftright[1000]{\langle}{\begin{array}{l}
			\ESC{\mathit{PRE}(x,\alpha)}\texttt{;}\\
			\texttt{match}\:  x\: \texttt{with}\\
				\qquad \texttt{[}\: D\: y\: \texttt{->}\\
				\qquad \qquad \ESC{\mathit{PRE}(y,\alpha . D)}\texttt{;}\\
				\qquad \qquad \ESC{\mathit{FIN}(y,\alpha . D)}\\
				\qquad \texttt{|}\: \texttt{\_}\: \texttt{->}\: \texttt{\{\}}\: \texttt{(* do nothing *)}\\
				\qquad \texttt{]}
		\end{array}}{\rangle} \\
	\texttt{else}\\
		\qquad \langle\ESC{\mathit{PRE}(x,\alpha)}\texttt{;}\: \ESC{\mathit{FIN}(x,\alpha)}\rangle
	\end{array}
\\

\mathit{SCAN}\left(\!\!\!
	\begin{array}{c}
		x,\alpha,p \cdot p'\!,\\
		\mathit{PRE},\mathit{FIN}
	\end{array}\!\!\!\right) =
	\begin{array}[t]{l}
		\langle\ESC{\mathit{SCAN}(x,\alpha,p,\mathit{PRE},\mathit{CONT})}\rangle\\
		\quad \text{where $\mathit{CONT}$ is defined by}\\
		\qquad \mathit{CONT}(y, \alpha ') = \mathit{SCAN}(y,\alpha',p',\mathit{PRE},\mathit{FIN})
	\end{array}
\\

\mathit{SCAN}\left(\!\!\!
	\begin{array}{c}
		x,\alpha,p + p'\!,\\
		\mathit{PRE},\mathit{FIN}
	\end{array}\!\!\!\right) =
	\stretchleftright[1000]{\langle}{\begin{array}{l}
		\ESC{\mathit{SCAN}(x,\alpha,p,\mathit{PRE},\mathit{FIN})}\texttt{;}\\
		\ESC{\mathit{SCAN}(x,\alpha,p',\mathit{PRE},\mathit{FIN})}
	\end{array}}{\rangle}
\\

\mathit{SCAN}\left(\!\!\!
	\begin{array}{c}
		x,\alpha,p^{*}\!,\\
		\mathit{PRE},\mathit{FIN}
	\end{array}\!\!\!\right) =
	\begin{array}[t]{l}
		\langle\mathit{loop}\texttt{(}x\texttt{)}\rangle\\
		\quad \text{where $\mathit{loop}$ is a fresh function defined by}\\
		\qquad \begin{array}{l}
			\texttt{fun}\: \mathit{loop}\texttt{(}x\texttt{)} \texttt{=}\\
			\qquad \stretchleftright[1000]{\langle}{\begin{array}{l}
				\ESC{\mathit{SCAN}(x,\alpha.p,p,\mathit{PRE},\mathit{LOOP})}\texttt{;}\\
				\ESC{\mathit{SCAN}(x,\alpha,\epsilon,\mathit{PRE},\mathit{FIN})}
			\end{array}}{\rangle}
		\end{array}\\
		\quad \text{and $\mathit{LOOP}$ is a staged function defined by}\\
		\qquad\mathit{LOOP}(x, \alpha) = \langle\mathit{loop}\texttt{(}x\texttt{)}\rangle
	\end{array}

\end{array}
\end{displaymath}
\caption{Definition of the $\mathit{SCAN}$ macro.}
\label{fig:scan-def}
\end{figure}



\subsection{Optimisation of $\mathit{SCAN}$ning code}

Note that Figure~\ref{fig:scan-def} above defines a simple, general definition of $\mathit{SCAN}$.
The code generated by this simple version is not optimised.
We now give an overview of different possible improvements.

First, consider the code generated for the repetition operator $\mathit{SCAN}(x,\alpha,p^{*}\!,\nolinebreak[1]\mathit{PRE},\nolinebreak[1]\mathit{FIN})$: a recursive function.
Note that the recursive calls (the ones generated by $\mathit{SCAN}(x,\alpha.p,p,\mathit{PRE},\mathit{LOOP})$) are not in tail position (i.e., not immediately before $\texttt{return}$) which prevents tail-call optimisation – also known as tail-call elimination.
It is tempting to swap the two lines of the definition of $\mathit{loop}$ to enable tail-call optimisation.
However, note that there might be multiple sequential recursive calls – e.g., if the path $p$ is a disjunction $p_1 + p_2$ – only one of which would be in tail position.
Also note that, even with tail-call-friendly paths, swapping the two lines is not always safe.
Indeed, consider the case where $\mathit{FIN}$ is used to deallocate the memory: the block representing $x$ would be deallocated before it is used.

Second, consider that we only require $\mathit{PRE}$ to be idempotent; we made no further hypotheses about $\mathit{FIN}$ and $\mathit{PRE}$.
In Chapter~\ref{chap:asap}, we see that $\mathit{SCAN}$ receives specific instantiations of $\mathit{FIN}$ and $\mathit{PRE}$.
Specifically, either $\mathit{FIN}$ or $\mathit{PRE}$ is a no-op.
Thus, the code generated by $\mathit{SCAN}$ can be simplified.

Third, consider paths that are disjunction of discriminant names such as $D_1 + \dots + D_n$.
As presented above, $\mathit{SCAN}$ generates a sequence of $n$ matches, at most one of which will succeed.
Additionally, the generated code uses $\mathit{PRE}${} $n$ times.
Instead, the function can generate a single match, at most one branch of which will be executed, as follows.
\[
	\stretchleftright[1000]{\langle}{\begin{array}{l}
		\ESC{\mathit{PRE}(x,\alpha)}\texttt{;}\\
		\texttt{match}\:  x\: \texttt{with}\\
			\qquad \texttt{[}\: D_1\: x'\: \texttt{->}\: \ESC{\mathit{PRE}(x',\alpha . D)}\texttt{;}\: \ESC{\mathit{FIN}(x',\alpha . D)}\\
			\qquad \texttt{|}\: \dots\\
			\qquad \texttt{|}\: D_n\: x'\: \texttt{->}\: \ESC{\mathit{PRE}(x',\alpha . D)}\texttt{;}\: \ESC{\mathit{FIN}(x',\alpha . D)}\\
			\qquad \texttt{|}\: \texttt{\_}\: \texttt{->}\: \texttt{\{\}}\: \texttt{/* do nothing */}\\
			\qquad \texttt{]}
	\end{array}}{\rangle}
\]

